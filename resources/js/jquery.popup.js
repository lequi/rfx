	 function initPopup(){

		$(".topopup").click(function() {
				loading(); // loading
				setTimeout(function(){ // then show popup, deley in .5 second
					loadPopup(); // function show popup 
				}, 500); // .5 second
		return false;
		});
	
		/* event for close the popup */
		$("div.close").hover(
						function() {
							$('span.ecs_tooltip').show();
						},
						function () {
	    					$('span.ecs_tooltip').hide();
	  					}
					);
	
		$("div.close").click(function() {
			disablePopup();  // function close pop up
		});

		$("#btnCancel").click(function() {
			disablePopup();  // function close pop up
		});

		$(this).keyup(function(event) {
			if (event.which == 27) { // 27 is 'Ecs' in the keyboard
				disablePopup();  // function close pop up
			}  	
		});
	
		$("div#backgroundPopup").click(function() {
			disablePopup();  // function close pop up
		});
	
		$('a.livebox').click(function() {
			alert('Hello World!');
		return false;
		});
	
	};
	// end init
	 /************** prepare content *****************/
	
	function prepareContent(worker, partition, topic){
		var parts = partition.split("-");
		var beginPartition = parseInt(parts[0]);
		var endPartition = parseInt(parts[1]);
		
		// get offset
		var uri = "ajax?action=get_offset&kafka_topic=" +topic + "&worker_name=" + worker ;
		$.ajax(uri, {
			    type: 'POST', 
			    dataType: 'json',
			    success: function(data) {
				if (data.status === 'ok')
				{
				  
				    var jsonOffset = JSON.parse(data.msg); 
				    var html = "";
				    for(var i= beginPartition; i<= endPartition; i++){  
					var key = "Client_" + topic + "_" + i;
					var offsetVal = jsonOffset[key];
					html += "<div class=\"row\">";
					html += "<div class=\"col\">"+ i +"</div>";
					html += "<div class=\"col\"><input type=\"textbox\" id=\"offset"+ i +"\" value=\""+ offsetVal +"\" /></div>";
					html += "</div>";
				  }
	
				  var func = "setOffsetPartition('"+ worker +"','"+ topic +"','"+ beginPartition +"','"+ endPartition +"')";
				  $("#btnSubmitOffset").attr("onclick", func);
				  $("#offset-content").html(html);

				  loadPopup(); // function show popup 


				}
				else {
				    alert(data.msg, "Lỗi");
				}

			}

			
		}); // ajax		

		
	}	

	function setOffsetPartition(worker,topic, beginPartition, endPartition){
		var r = confirm("Are you sure ? ");
		if(r){
			var data = "";
			for(var i= beginPartition; i<= endPartition; i++){
				var val = $("#offset" +i).val();
				data += "," + i + ":" + val;
			}
			if(data.length > 0){
				data = data.substring(1);			
			}
			
			var uri = "ajax?action=set_offset&worker_name=" + worker + "&kafka_topic=" +topic + 
				  "&begin_partition=" + beginPartition + "&end_partition=" +endPartition + 					  "&data_offset=" +data;			 

			$.ajax(uri, {
				    type: 'POST', 
				    dataType: 'json',
				    success: function(data) {
					if (data.status === 'ok')
					{
					    console.log(" response is :" + data.status);
					    disablePopup(); 
					}
					else {
					    alert(data.msg, "Lỗi");
					}
				}
			}); // ajax		
			console.log(data);
		}else{
		     disablePopup(); 
		}	
			
	}	

	 /************** start: functions. **************/
	function loading() {
		$("div.loader").show();  
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');  
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#toPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, close popup
			$("#toPopup").fadeOut("normal");  
			$("#backgroundPopup").fadeOut("normal");  
			popupStatus = 0;  // and set value to 0
		}
	}
	/************** end: functions. **************/


	var showPopup = function(worker, partition, topic) {
			prepareContent(worker, partition, topic);
			
			loading(); // loadin
			 /*setTimeout(function(){ // then show popup, deley in .5 second
					loadPopup(); // function show popup 
				  }, 500); // .5 second
			*/
			return false;
	};
		


	var workerSetup = window.workerSetup || {};
	(function ($) {
	    workerSetup = {
		workerInfo: "",
		kafkaTopics: "",
		init: function(){
			workerSetup.loadKafkaTopic();
			workerSetup.selectWorkerHandle();
			workerSetup.bindWorkerToKafkaTopic();
			workerSetup.bindingRestartSystem();
		},
		loadWorkerToList : function(workerInfo){
			var html = "<option ref='-1' value=''>Choose worker</option>";
			for(var i in workerInfo){
				html += "<option ref='"+ i +"' value='"+ workerInfo[i].worker_name +"'>"+ 
					workerInfo[i].worker_name +"</option>";
			}
			$("#slWorker").html(html);
			workerSetup.workerInfo = workerInfo;
		},

		selectWorkerHandle : function(){
			$("#slWorker").change(function() {
    				var option = $(this).find('option:selected');
				var index = option.attr('ref');
				var workerName = option.val();
				
				console.log(index);
				workerSetup.setWorkerInfo(workerSetup.workerInfo[index]);
				workerSetup.setPartitionRange(workerSetup.workerInfo[index]);

			});	
		},

		loadKafkaTopic: function(){
			var uri = "json/get-kafka-topic";		 
			$.getJSON( uri, function(listWorkers){
				  workerSetup.kafkaTopics = listWorkers.kafkaTopics;
			});
		},

		setWorkerInfo : function(targetWorker){
			var kafkaTopics = workerSetup.kafkaTopics;
			var html = "";
			var kt = false;
			for(var i in kafkaTopics){
				
				if(targetWorker.kafka_topic == kafkaTopics[i]){
			  	        html += "<option value='"+ kafkaTopics[i] +"' selected='selected'>"+ 
					kafkaTopics[i] +"</option>";	
					kt = true;	
				}
				else{
					html += "<option value='"+ kafkaTopics[i] +"'>"+ 
					kafkaTopics[i] +"</option>";
				}
			}
			if(!kt){
				html = "<option value=''>Choose topic</option>" + html;
			}
			$("#slTopic").html(html);
			
		},

		setPartitionRange : function(targetWorker){
			
			var partitions = targetWorker.partition;
			if(partitions != ""){
				var parts = partitions.split("-");
				var beginPartition = parseInt(parts[0]);
				var endPartition = parseInt(parts[1]);
			
				$('#txtBeginPartition').val(beginPartition);
				$('#txtEndPartition').val(endPartition);
			}
			else{
				$('#txtBeginPartition').val(0);
				$('#txtEndPartition').val(0);
			}
			
		},


		bindWorkerToKafkaTopic: function(){
			$("#btnSubmitWorker").click(function() {

				var r = confirm("Are you sure ? ");
				if(r){
					$(this).attr("id", "");
					$("#imgLoading").css("display","block");

					var worker = $('#slWorker').val();
					var topic = $('#slTopic').val();
					var beginPartition = $('#txtBeginPartition').val();
					var endPartition = $('#txtEndPartition').val();
					
					if(worker == ""){
					    alert("Warning: Choose worker");
					    return;
					}
					if(topic == ""){
					    alert("Warning: Choose topic");
					    return;
					}
					
					if(beginPartition < 0 || endPartition < 0 || beginPartition >= endPartition){
					    alert("Warning: Partition range is not regular");
					    return;
					}

					console.log(worker + ","+ topic + ",{" + beginPartition, "," + endPartition + "}" );
	
					var uri = "ajax?action=set_worker_topic&worker_name=" + worker + "&kafka_topic=" +topic + 
						  "&begin_partition=" + beginPartition +
						  "&end_partition=" +endPartition;			 

					$.ajax(uri, {
						    type: 'POST', 
						    dataType: 'json',
						    success: function(data) {
							if (data.status === 'ok')
							{
							    console.log(" response is :" + data.status);
							    alert("Successfully");
							}
							else {
							    alert(data.msg, "ERROR");							   
							}
 							window.location = window.location.href;
						}
					}); // ajax		
		
				}	
			}); // click
		},

		bindingRestartSystem: function(){
			$("#lnkRestartSys").click(function() {
				var r = confirm("Are you sure ? ");
				if(r){
					var uri = "ajax?action=kill_all";	
					$.ajax(uri, {
						    type: 'POST', 
						    dataType: 'json',
						    success: function(data) {
							if (data.status === 'ok')
							{
							    console.log(" response is :" + data.status);
							    alert("Successfully");
							}
							else {
							    alert(data.msg, "ERROR");							   
							}
							window.location = window.location.href;
						}
					}); // ajax	
				}
			});
			
		}

	    }
	})(jQuery);

	$(document).ready(function(){
		initPopup();
		workerSetup.init();
	});



