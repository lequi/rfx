package test.vertx;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.VertxFactory;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;

import rfx.core.model.Callback;
import rfx.core.model.CallbackResult;
import rfx.core.model.http.HttpServerRequestCallback;
import rfx.core.util.Utils;

public class HttpServerStarter {

	static Map<String, HttpServerRequestCallback> callbackHandlers = new HashMap<String, HttpServerRequestCallback>();

	static {
		callbackHandlers.put("/", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {	
				String name = request.params().get("name");
				return "hello "+name;
			}			
		});
		callbackHandlers.put("/tracking", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {				
				return "tracked";
			}		
			@Override
			protected String onFiltered() {				
				return "tracking is rejected";
			}
		});
		callbackHandlers.put("/give-a-error", new HttpServerRequestCallback(){
			@Override
			protected String onHttpGetOk() {
				String error = request.params().get("error");
				if(error != null){
					throw new IllegalArgumentException("you got the IllegalArgumentException");
				}
				return "no error";
			}
			@Override
			protected String onError(Throwable e) {
				
				return "implemented error got "+e.getMessage();
			}
		});
	}

	public static void main(String[] args) {

		Vertx vertx = VertxFactory.newVertx();

		HttpServer server = vertx.createHttpServer();

		server.requestHandler(new Handler<HttpServerRequest>() {
			public void handle(HttpServerRequest request) {
				boolean isPost = false;
				if("POST".equals(request.method().toUpperCase())){
					request.expectMultiPart(true);
					isPost = true;
				}
				request.response().putHeader("content-type", "text/plain");
				System.out.println(request.path());
				System.out.println(request.params().entries());
				System.out.println(request.headers().entries());
				
				HttpServerRequestCallback callback = callbackHandlers.get(request.path().toLowerCase());
				if(callback != null){
					request.response().end(callback.handleHttpGetRequest(request).getResult());
				} else {
					request.response().end("not support!");
				}
				
			}
		});

		server.listen(8888, "localhost");

		final AtomicInteger c = new AtomicInteger(0);
		// Prevent the JVM from exiting
		Utils.foreverLoop(1000, new Callback<Boolean>() {
			@Override
			public CallbackResult<Boolean> call() {
				if(c.incrementAndGet() > 100){
					//working for 100 seconds and die
					return null;	
				}
				return new CallbackResult<Boolean>(false);
			}
		});
	}
}
