package test.rfx.config;

import java.io.File;
import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.configs.RedisPoolConfigs;
import rfx.core.configs.ScheduledJobConfig;
import rfx.core.configs.WorkerConfigs;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.nosql.jedis.RedisConnectionPoolConfig;
import rfx.core.nosql.jedis.Shardable;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;



public class TestXmlConfig {

	static void sampleParse() {
		try {
			XMLConfiguration config = new XMLConfiguration("configs/cluster.xml");

			System.out.println(config.getString("worker.start-shellpath"));
			System.out.println(config.getFloat("worker.number", 0) == 2.1f);
		} catch (ConfigurationException cex) {
			cex.printStackTrace();
		}

		try {
			File input = new File("configs/cluster.xml");
			Document doc = Jsoup.parse(input, "UTF-8");
			System.out.println(doc.select("worker start-shellpath").text());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void testScheduledJobConfig(){
		ScheduledJobConfig jobConfig = ScheduledJobConfig.getConfigs().get(0);		
		System.out.println("ScheduledJobConfig.getConfigs(): "+ ScheduledJobConfig.getConfigs());
		System.out.println("jobConfig: "+ jobConfig.getClasspath());
		System.out.println("jobConfig.getDelay: "+ jobConfig.getDelay());
		
		jobConfig = ScheduledJobConfig.getConfigs().get(0);		
		System.out.println("jobConfig: "+ jobConfig.getClasspath());
		System.out.println("jobConfig.getDelay: "+ jobConfig.getDelay());
	}
	
	static void testClusterInfoConfigs(){
		System.out.println("-------ClusterInfoConfigs---------");
		System.out.println("getClusterInfo: "+ ClusterInfoConfigs.load().getClusterInfoRedis());
		System.out.println("getKafkaOffsetDbPath: "+ WorkerConfigs.load().getKafkaOffsetDbPath());		
	}

	static void testWorkerConfigs(){
		WorkerConfigs workerConfigs = WorkerConfigs.load();
		System.out.println("-------workerConfigs---------");
		System.out.println(workerConfigs.getHostName());
		System.out.println(workerConfigs.getStartWorkerScriptPath());
		System.out.println(workerConfigs.getHostName());
		System.out.println(workerConfigs.getAllocatedWorkers());
		LogUtil.i("", "info test", true);
		LogUtil.e("", "error test");
	}
	
	static void testKafkaTopologyConfig(){
		System.out.println(KafkaTopologyConfig.getTopologyClassPath("video-tracking"));
		System.out.println(KafkaTopologyConfig.getTopologyClassPath("pageview-impression"));
		System.out.println(KafkaTopologyConfig.getTopologyClassPath("m-click"));
	}
	
	static class Creative implements Shardable {
		int id;		
		public Creative(int id) {
			super();
			this.id = id;
		}
		@Override
		public Number getShardKey() {			
			return id;
		}		
	}
	
	static void testRedisPoolConfigs(){		
		
		RedisPoolConfigs redisPoolConfigs = RedisPoolConfigs.load(); 		
		System.out.println("-------redisPoolConfigs---------");	
		System.out.println("getRedisPools: "+ redisPoolConfigs.getRedisPools());
		
		
		System.out.println("getRedisConnectionPoolConfig().getMaxActive: "+  RedisConnectionPoolConfig.theInstance().getMaxTotal());
	}

	public static void main(String[] args) {
		ConfigAutoLoader.loadAll();
		testClusterInfoConfigs();
		testRedisPoolConfigs();
		testWorkerConfigs();
		Utils.sleep(1000);
	}
}
