package test.rfx.rx;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;
import rx.util.functions.Action0;
import rx.util.functions.Action1;

public class TestRxBufferStream {
	
	static Queue<String> stream = new ConcurrentLinkedQueue<String>();
	
	static Runnable producer1 = new Runnable() {		
		@Override
		public void run() {
			for (int i = 0; i < 50; i++) {				
				stream.add("event:"+i);		
				if(i % 10 == 0){
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {}
				}
			}
		}
	};
	
	static Runnable producer2 = new Runnable() {		
		@Override
		public void run() {
			for (int i = 50; i < 112; i++) {				
				stream.add("event:"+i);		
				if(i % 10 == 0){
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {}
				}
			}
		}
	};
	
	static Runnable dumpSize = new Runnable() {		
		@Override
		public void run() {			
			System.out.println("size of stream: " + stream.size());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}				
			
		}
	};

	static AtomicInteger counter = new AtomicInteger(0);
	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		executor.execute(dumpSize);
		
		executor.execute(producer1);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {}		
		
		executor.execute(producer1);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {}	
				
	
		
		while(true)		
		{			
			Observable<String> oStream = Observable.from(stream);			
//			Observable.buffer(oStream, 10).subscribe(new Action1<List<String>>() {						
//				@Override
//				public void call(List<String> list) {
//					System.out.println("batch:" + list);
//					counter.addAndGet(list.size()) ;					
//				}
//			}, new Action1<Throwable>() {			
//				@Override
//				public void call(Throwable arg0) {
//					System.err.println(" on error: "+ arg0.getMessage());
//				}
//			}, new Action0() {			
//				@Override
//				public void call() {
//					System.out.println("done! "+ counter.get());
//				}
//			});
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {}		
			if(counter.get() >= stream.size() || stream.isEmpty()){
				break;				
			}			
		}
		
		
		
		
		System.exit(1);
		
		//
	}
}
