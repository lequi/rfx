package test.rfx.caching;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rfx.core.util.Utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class TestGuavaCache {

	final static int CACHE_TIME_OUT = 4;// SECONDS
	final static int CACHE_MAX_SIZE = 100000;

	public static class UrlMapperKey {
		String url = "";
		int pubId;

		public UrlMapperKey(String url, int pubId) {
			super();
			this.url = url;
			this.pubId = pubId;
		}

		public String getUrl() {
			return url;
		}

		public int getPubId() {
			return pubId;
		}

		@Override
		public String toString() {
			return url + "-" + pubId;
		}
	}

	// local cache for campaign details (Use Java Google Cache)
	static CacheLoader<String, List<String>> cacheLoader = new CacheLoader<String, List<String>>() {
		/*
		 * if cache not found, this method will be executed to get from Redis
		 * 
		 * @see com.google.common.cache.CacheLoader#load(java.lang.Object)
		 */
		public List<String> load(String key) {
			return new ArrayList<>(0);
		}
	};

	static RemovalListener<String, List<String>> removalListener = new RemovalListener<String, List<String>>() {
		public void onRemoval(RemovalNotification<String, List<String>> removal) {						 
			System.out.println("clear: "+removal.getKey() + " "+removal.getValue());
			//list.clear();
		}
	};
	static LoadingCache<String, List<String>> urlMapperCache = CacheBuilder
			.newBuilder().maximumSize(2)
			.expireAfterWrite(CACHE_TIME_OUT, TimeUnit.SECONDS)
			.removalListener(removalListener)
			.build(cacheLoader);

	public final static List<String> getCategoryFromRedis(String url, int pubId) {
		List<String> result = new ArrayList<String>(2);
		result.add(url + "-" + System.currentTimeMillis());
		result.add("" + pubId);
		return result;
	}

	public static void main(String[] args) {

		List<String> result = new ArrayList<String>(2);
		result.add("url-1");
		
		result.add("111");
		urlMapperCache.put("url-1" + 111, result);
		System.out.println(urlMapperCache.size());
		
		result.add("112");
		urlMapperCache.put("url-1" + 112, result);
		System.out.println(urlMapperCache.size());
		
		result.add("113");
		urlMapperCache.put("url-1" + 113, result);
		System.out.println(urlMapperCache.size());
		
		System.out.println("----keys:"+urlMapperCache.asMap().keySet());

		try {
			Thread.sleep(1000);
			System.out.println("after 1s:" + urlMapperCache.get("url-1" + 113));
			Thread.sleep(2000);
			System.out.println("after 3s:" +urlMapperCache.get("url-1" + 111));
			Thread.sleep(5000);
			System.out.println("after 8s:" +urlMapperCache.get("url-1" + 111));
			if (urlMapperCache.get("url-1" + 111).isEmpty()) {
				urlMapperCache.put("url-1" + 111, result);
				System.out.println("write new : "+result);
			}
			Thread.sleep(3000);
			System.out.println("after 3s:" +urlMapperCache.get("url-1" + 111));
			Thread.sleep(1000);
			System.out.println("after 4s:" +urlMapperCache.get("url-1" + 111));
		} catch (Exception e) {
			e.printStackTrace();
		}
		Utils.sleep(5000);
		
	}
}
