package test.rfx.actor;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

import rfx.core.nosql.graph.Digraph;
import rfx.core.util.Utils;
import rfx.core.util.io.StdOut;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class ActiveFunctorGraph {
	
	static AtomicInteger counter = new AtomicInteger(0);
	static int MAX_POOL_SIZE = 20;
	private static final int MIN = 0;
	private static final int MAX = MAX_POOL_SIZE - 1;	
	
	public static class Message implements Serializable {
		private static final long serialVersionUID = -3510557864155279314L;
		public final String msg;
		public Message(String msg) {
			this.msg = msg;
		}
	}
	
	static final int randomActorId(){			
		int partition = 0 + (int)(Math.random() * ((MAX - MIN) + 1));
		return partition;
	}
	
	public static class Phase2 extends UntypedActor {
		LoggingAdapter log = Logging.getLogger(getContext().system(), this);
		public void onReceive(Object message) throws Exception {
			if (message instanceof Message){
				log.info("Phase2 got msg: " + ((Message) message).msg);
			}
		}
	}

	public static class Phase1 extends UntypedActor {
		LoggingAdapter log = Logging.getLogger(getContext().system(), this);
		public void onReceive(Object message) throws Exception {
			if (message instanceof Message){
				log.info("Phase1 got msg: " + ((Message) message).msg);
			}
		}
	}
	
	
	 static void testTopologyGraph2(){    	
  	  	String[] actorPool1 = new String[] {"a1", "a2", "a3"};
  	  	String[] actorPool2 = new String[] {"b1", "b2", "b3"};
  	  	String[] actorPool3 = new String[] {"c1", "c2", "c3"};
  	  	int size = actorPool1.length +  actorPool2.length +  actorPool3.length;
  	  	Digraph<String> dg = new Digraph<>(size);  	  
  	  
  	  	//create graph topology
  	  	for (String pubActor : actorPool1) {
  	  		for (String subActor : actorPool2) {
  	  			dg.addEdge(pubActor, subActor, Utils.randomNumber(1, 5));
  	  		}			
		}
  		for (String pubActor : actorPool1) {
  	  		for (String subActor : actorPool2) {
  	  			dg.addEdge(pubActor, subActor, Utils.randomNumber(1, 5));
  	  		}			
		}
  	  	for (String pubActor : actorPool2) {
  	  		for (String subActor : actorPool3) {
  	  			dg.addEdge(pubActor, subActor, Utils.randomNumber(1, 5));
  	  		}
		}  	  	
  	  	
  	  	StdOut.println();
		StdOut.println(dg);       
        
        //test, try remove a down connection, stop pub-sub a1->b2
        dg.removeEdge("a1", "b2");
        StdOut.println("removed a1 -> b2 ");
        
        StdOut.println();
    	StdOut.println(dg);
    	
    	dg.addEdge("a1", "b2", 12);
    	dg.addEdge("a1", "b1", 15);
    	StdOut.println("added a1 -> b2 ");
    	StdOut.println();
     	StdOut.println(dg);
    }
    
    static void testTopologyGraph1(){    	
    	
    }


    /**
     * Unit tests the <tt>Digraph</tt> data type.
     */
    public static void main(String[] args) {
    	testTopologyGraph2();
    }
}
