package test.rfx.lucene;


import java.io.File;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class TestSeach {
	public static void main(String[] args) throws Exception {
		Version version = Version.LUCENE_47;
		
		Analyzer analyzer = new StandardAnalyzer(version);

		// Store the index in memory:
		// Directory directory = new RAMDirectory();
		boolean create = false;
		File indexDirFile = new File("data/lucene-index");
		if ( ! indexDirFile.exists() || !indexDirFile.isDirectory()) {
			create = indexDirFile.mkdir();
		} else {
			create = true;
		}
		
		if( !create ){
			return;
		}

		// To store an index on disk
		Directory directory = FSDirectory.open(indexDirFile);
		
		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);
		IndexWriter iwriter = new IndexWriter(directory, config);
		Document doc = new Document();
		String text = "This is the text to be indexed.";
		doc.add(new Field("fieldname", text, TextField.TYPE_STORED));
		iwriter.addDocument(doc);
		iwriter.close();

		// Now search the index:
		DirectoryReader ireader = DirectoryReader.open(directory);
		IndexSearcher isearcher = new IndexSearcher(ireader);
		// Parse a simple query that searches for "text":
		QueryParser parser = new QueryParser(version,"fieldname", analyzer);
		Query query = parser.parse("text");
		ScoreDoc[] hits = isearcher.search(query, null, 1000).scoreDocs;
		System.out.println(1 == hits.length);
		// Iterate through the results:
		for (int i = 0; i < hits.length; i++) {
			Document hitDoc = isearcher.doc(hits[i].doc);
			System.out.println("This is the text to be indexed.".equals(hitDoc.get("fieldname")));
		}
		ireader.close();
		directory.close();
	}
}

