package test.rfx.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import rfx.core.util.HttpClientUtil;

public class TestHttpClient {
	private static final int NTHREDS = 30;

	public static void main(String[] args) throws InterruptedException {
		final String url = "http://t.d.adx.vn/a1/pi?bc=90ac7a70c4,a9b52491981336f8ce423160,1410536118,8ce4c78299f0128f,1,512020806,512020131,512020134,10383,1,512031562,512030321,1&res=1366x768&oid=&gd=3&loc=24-2-vn";

		final AtomicInteger c = new AtomicInteger();

		ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
		for (int i = 0; i < 10000; i++) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					HttpClientUtil.executeGet(url, true);
					System.out.println(c.incrementAndGet());
				}
			});
		}
		// This will make the executor accept no new threads
		// and finish all existing threads in the queue
		executor.shutdown();
		// Wait until all threads are finish
		executor.awaitTermination(1L, TimeUnit.HOURS);
		System.out.println("Finished all threads");
	}
}
