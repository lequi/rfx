package test.rfx.algorithms;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rfx.core.util.io.FileUtils;

import com.google.common.base.Stopwatch;

public class Multiply2Matrix {

	static Double[][] A = { { 4.00, 3.00, 2.00 }, { 2.00, 1.00 , 2.00 }, { 2.00, 1.00 , 2.00 } };
	static Double[][] B = { { -0.500, 1.500 , 2.00}, { 1.000, -2.0000 ,2.00 }, { 1.000, -2.0000 ,2.00 } };
	
	static {
		try {
			String s = FileUtils.readFileAsString("matrix-10x10.txt");
			
			
			System.out.println(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static Double[][] multiplicar(Double[][] A, Double[][] B) {

		int aRows = A.length;
		int aColumns = A[0].length;
		int bRows = B.length;
		int bColumns = B[0].length;

		if (aColumns != bRows) {
			throw new IllegalArgumentException("A:Rows: " + aColumns
					+ " did not match B:Columns " + bRows + ".");
		}

		Double[][] C = new Double[aRows][bColumns];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				C[i][j] = 0.00000;
			}
		}

		for (int i = 0; i < aRows; i++) { // aRow
			for (int j = 0; j < bColumns; j++) { // bColumn
				for (int k = 0; k < aColumns; k++) { // aColumn
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}

		return C;
	}
	
	static Random random = new Random();
	static double generateRandomNumber(){
		double number = random.nextDouble()*10000;
		return Math.round(number*1000.0)/1000.0;
	}
	
	static void generateMatrixFile(int rows, int cols){
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double n = generateRandomNumber();
				s.append(n).append(" ");
			}
			s.append("\n");
		}
		String rs = s.toString();
		FileUtils.writeStringToFile("matrix-"+rows+"x"+cols+".txt", rs);
		System.out.println(rs);
	}

	public static void main(String[] args) {
		//generateMatrixFile(10,10);
		
		

		Multiply2Matrix matrix = new Multiply2Matrix();
		
		Stopwatch stopwatch = new Stopwatch().start();
		Double[][] result = multiplicar(matrix.A, matrix.B);

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++)
				System.out.print(result[i][j] + " ");
			System.out.println();
		}
		System.out.println("totalTime in milliseconds is "+stopwatch.elapsedTime(TimeUnit.MILLISECONDS));
	}

}
