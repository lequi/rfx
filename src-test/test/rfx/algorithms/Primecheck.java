package test.rfx.algorithms;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class Primecheck {
	public static void main(String args[]) {
		Stopwatch stopwatch = new Stopwatch().start();
		
		int lowerBound = 2, higherBound = 100000;
		boolean primecheck;

		ArrayList<Integer> primes = new ArrayList<Integer>();
		if ((lowerBound >= 2) && (higherBound > lowerBound)) {
			int k;
			for (k = lowerBound; k < higherBound; k++) {
				primecheck = true;
				for (int j = 2; j < k / 2; j++) {
					if (k % j == 0) {
						primecheck = false;
						break;
					}
				}
				if (primecheck) {
					primes.add(k);
				}
			}
		} else {
			// invalid bounds
			return;
		}
		if (primes.size() > 0) {
			for (int num : primes) {
				System.out.println(num);
			}
		} else {
			System.out.println("No primes exist");
		}
		
		System.out.println("totalTime in milliseconds is "+stopwatch.elapsedTime(TimeUnit.MILLISECONDS));
	}
}