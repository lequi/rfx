package rfx.sample.user.tracking.functors;

import rfx.core.functor.BaseFunctor;
import rfx.core.listener.Metricable;
import rfx.core.message.Fields;
import rfx.core.message.Tuple;
import rfx.core.message.Values;
import rfx.core.model.DataFlowInfo;
import rfx.core.processor.Processor;
import rfx.core.topology.BaseTopology;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class UserTrackingLogTokenizer extends BaseFunctor implements Metricable {

	// what data fields that this actor would send to next actor
	static Fields outputFields = new Fields("loggedtime", "uuid", "event","url", "topic", "partitionId");
	UserTrackingLogProcessor processor;

	public UserTrackingLogTokenizer(DataFlowInfo dataFlowInfo,
			BaseTopology topology) {
		super(dataFlowInfo, topology);
		processor = new UserTrackingLogProcessor();
	}

	public void onReceive(Object message) throws Exception {
		if (message instanceof Tuple) {
			this.doPreProcessing();
			Tuple inputTuple = (Tuple) message;
			Tuple outTuple = processor.processToTuple(inputTuple, outputFields);

			if (outTuple != null) {
				// output to next phase
				this.emit(outTuple, self());
				
				String monitorkey = StringUtil.toString(this.getMetricKey(),outTuple.getStringByField("topic"), "#", outTuple.getIntegerByField("partitionId"));
				this.counter(monitorkey).incrementAndGet();
				this.topology.counter().incrementAndGet();
			} else {
				LogUtil.error("logTokens.length (delimiter is tab) is NOT = 5, INVALID LOG ROW FORMAT ");
			}
			inputTuple.clear();
		} else {
			unhandled(message);
		}
	}

	@Override
	public String getMetricKey() {
		return "TokenizingActor#";
	}
	
	static class UserTrackingLogProcessor extends Processor{

		@Override
		public Tuple processToTuple(Tuple inputTuple, Fields outputMetadataFields) {
			String logRow = inputTuple.getStringByField("log_row");
	    	String topic = inputTuple.getStringByField("topic");
	    	String partitionId = inputTuple.getStringByField("partitionId");	    	
			String[] logTokens = logRow.split("\t");		  	

			if(logTokens.length == 4){
				if(	   StringUtil.isNotEmpty(logTokens[0])
					&& StringUtil.isNotEmpty(logTokens[1]) 
					&& StringUtil.isNotEmpty(logTokens[2]) 
					&& StringUtil.isNotEmpty(logTokens[3])
					){
					
					long loggedtime = StringUtil.safeParseLong(logTokens[0]);
					String uuid = StringUtil.safeString(logTokens[1]);
					String event = logTokens[2];
					String url = StringUtil.safeString(logTokens[3]);	
				
					return new Tuple(outputMetadataFields, new Values(loggedtime ,uuid ,event, url ,topic ,partitionId));
				}
			}	
			return null;
		}

		@Override
		public boolean processTuple(Tuple inputTuple) {
			return false;
			//skip
		}
	}
}