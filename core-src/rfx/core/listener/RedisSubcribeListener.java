package rfx.core.listener;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.nosql.jedis.RedisConnectionPoolConfig;
import rfx.core.util.StringUtil;

import com.google.gson.Gson;

public class RedisSubcribeListener extends JedisPubSub {
	public static final String CHANNEL_NAME = "commonChannel";
	//private static Logger logger = LoggerFactory.getLogger(RedisSubcribeListener.class);
	static ClusterInfoConfigs configs = ClusterInfoConfigs.load();
	static ShardedJedisPool jedisPool = ClusterInfoConfigs.load().getClusterInfoRedis().getShardedJedisPool();
	ChannelGroup allChannels;
		
	public RedisSubcribeListener(ChannelGroup allChannels) {
		this.allChannels = allChannels;
		init();		
	}
	
	public synchronized ChannelGroup getAllChannels() {
		return allChannels;
	}
	
	public synchronized void addChannel(Channel channel) {
		allChannels.add(channel);
	}
	
	void init(){
		System.out.println("init RedisSubcribeListener");
	
		new Timer().schedule(new TimerTask(){
			@Override
			public void run() {
				try {
					System.out.println("Subscribing to \"commonChannel\". This thread will be blocked.");
					
					JedisPoolConfig poolConfig = RedisConnectionPoolConfig.getJedisPoolConfigInstance();	        
			        String host = configs.getClusterInfoRedis().getHost();
					int port = configs.getClusterInfoRedis().getPort();		
			        JedisPool jedisPool = new JedisPool(poolConfig, host, port, 0);			 
			        Jedis subscriberJedis = jedisPool.getResource();
					subscriberJedis.subscribe(RedisSubcribeListener.this, CHANNEL_NAME);
					
					System.out.println("Subscription ended.");
				} catch (Exception e) {
					System.err.println("Subscribing failed.");
					e.printStackTrace();
				}
			}
		}, 1);
		
		new Timer().schedule(new TimerTask() {			
			@Override
			public void run() {
				for (Channel webSocketChannel : allChannels) {
					Map<String, Object> monitorStats = new HashMap<String, Object>();					
					long pageview = (new RedisCommand<Long>(jedisPool) {
			            @Override
			            public Long build() throws JedisException {			            	
			                return StringUtil.safeParseLong(jedis.get("pageview"));
			            }
				    }).execute();	
					monitorStats.put("pageview", pageview);
					
//					String minuteStr = DateTimeUtil.formatDateHourMinute(new Date());
//					List<String> trendingKeywords = TrackingReportUtil.getTrendingKeywords(minuteStr);					
//					monitorStats.put("trending-keywords", StringUtil.joinFromList("\n", trendingKeywords));
//					
//					List<String> trendingUrls = TrackingReportUtil.getTrendingUrls(minuteStr);	
//					monitorStats.put("trending-urls", StringUtil.joinFromList("\n", trendingUrls));
										
					//ClusterDataManager.getWorkerData()
					
					webSocketChannel.write(new TextWebSocketFrame(new Gson().toJson(monitorStats)));
				}
			}
		}, 1000, 3000);
	}

	@Override
	public void onMessage(String channel, String message) {
		//logger.info("Message received. Channel: {}, Msg: {}", channel, message);

		for (Channel webSocketChannel : allChannels) {
			webSocketChannel.write(new TextWebSocketFrame(message));
		}
	}

	@Override
	public void onPMessage(String pattern, String channel, String message) {

	}

	@Override
	public void onSubscribe(String channel, int subscribedChannels) {

	}

	@Override
	public void onUnsubscribe(String channel, int subscribedChannels) {

	}

	@Override
	public void onPUnsubscribe(String pattern, int subscribedChannels) {

	}

	@Override
	public void onPSubscribe(String pattern, int subscribedChannels) {

	}
}