package rfx.core.emitters;

import rfx.core.listener.Metricable;
import rfx.core.model.DataFlowPreProcessing;

public abstract class DataEmitter extends DataFlowPreProcessing implements Metricable {
	
	public DataEmitter() {
		super();
	}	
}
