package rfx.core.nosql.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class Digraph<Item> {
    
    private int E;
    private UniqueSet<Item> vertices;
    private UniqueSet<Edge<Item>> edges;
    private Map<Item,Set<Item>> subscribedNodes;
    private Map<Item,Set<Item>> publishingNode;
    
    /**
     * Initializes an empty digraph with <em>V</em> vertices.
     * @param V the number of vertices
     * @throws java.lang.IllegalArgumentException if V < 0
     */
    public Digraph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices in a Digraph must be nonnegative");        
        this.E = 0;
        subscribedNodes = new HashMap<>(V);
        publishingNode = new HashMap<>(V);
        vertices = new UniqueSet<>(V);
        int maxEdge = V * (V - 1);
        edges = new UniqueSet<>(maxEdge);
    }

        
    /**
     * @return the number of vertices in the digraph
     */
    public int V() {
        return vertices.size();
    }

    /** 
     * @return the number of edges in the digraph
     */
    public int E() {
        return E;
    }


    /**
     * Adds the directed edge v->w to the digraph.
     * @param pubNode the tail vertex
     * @param subNode the head vertex 
     */
    public Edge<Item> addEdge(Item pubNode, Item subNode) {    	
    	return addEdge(pubNode, subNode, -1);
    }
    
    /**
     * Adds the directed edge v->w to the digraph.
     * @param pubNode the tail vertex
     * @param subNode the head vertex
     * @param weight the weight edge  
     */
    public Edge<Item> addEdge(Item pubNode, Item subNode, float weight) {
    	boolean edgeAdded = true;
    	Set<Item> outBag = subscribedNodes.get(pubNode);    	
    	if(outBag == null){
    		outBag = new TreeSet<>();
    		subscribedNodes.put(pubNode, outBag);
    	}
    	edgeAdded = edgeAdded && outBag.add(subNode);
    	
    	Set<Item> inBag = publishingNode.get(subNode);
    	if(inBag == null){
    		inBag = new TreeSet<>();
    		publishingNode.put(subNode, inBag);
    	}
    	edgeAdded = edgeAdded && inBag.add(pubNode);
    	Edge<Item> edge = null;
    	if(edgeAdded) {
        	vertices.add(subNode);
        	vertices.add(pubNode);
        	edge = new Edge<Item>(pubNode, subNode,weight);
        	edges.add(edge);
        	//System.out.println(edge+" hashCode edge "+edge.hashCode());
    		E++;    		
    	}
    	return edge;
    }
    
    
    public boolean removeEdge(Item pubNode, Item subNode) {    	
    	Set<Item> outBag = subscribedNodes.get(pubNode);
    	Set<Item> inBag = publishingNode.get(subNode);
    	if(outBag == null || inBag == null){
    		return false;
    	}
    	boolean edgeRemoved = true;
    	edgeRemoved = edgeRemoved && outBag.remove(subNode);
    	edgeRemoved = edgeRemoved && inBag.remove(pubNode);  
    	if(edgeRemoved){
    		Edge<Item> edge = new Edge<Item>(pubNode, subNode);
    		System.out.println("??:"+edges.contains(edge));
    		System.out.println(edge+" removed hashCode edge "+edge.hashCode());
    		boolean rm = edges.remove(edge);
    		System.out.println("rm:"+rm);
    		E--;
    	}
    	return edgeRemoved;
    }
       

    /**
     * Returns the vertices adjacent from vertex <tt>v</tt> in the digraph.
     * @return the vertices adjacent from vertex <tt>v</tt> in the digraph, as an Iterable
     * @param v the vertex
     */
    public Iterable<Item> allAdjacentVertices(Item v) {        
    	Set<Item> obag = subscribedNodes.get(v);
    	if(obag == null){
    		obag = new HashSet<Item>();
    	}
    	Set<Item> ibag = publishingNode.get(v);
    	if(ibag == null){
    		ibag = new HashSet<Item>();
    	}    	
    	return new Bag<Item>().addAll(ibag).addAll(obag);
    }
    
    public Iterable<Item> publishingNodesOf(Item v) { 
    	Set<Item> ibag = publishingNode.get(v);
    	if(ibag == null){
    		ibag = new HashSet<Item>();
    	}    	
    	return ibag;
    }
    
    public Iterable<Item> subscribedNodesOf(Item v) {        
    	Set<Item> obag = subscribedNodes.get(v);
    	if(obag == null){
    		obag = new HashSet<Item>();
    	}
    	return obag;
    }
    
    public Iterable<Item> vertices() {        
        return vertices;
    }
    
    public int degree(Item v){
    	Set<Item> obag = subscribedNodes.get(v);
    	if(obag == null){
    		obag = new HashSet<Item>();
    	}
    	Set<Item> ibag = publishingNode.get(v);
    	if(ibag == null){
    		ibag = new HashSet<Item>();
    	} 
    	return obag.size()+ibag.size();
    }
    
    public List<Edge<Item>> getSortedEdges(){
    	Collection<Edge<Item>> collection = this.edges;
 		List<Edge<Item>> list = new ArrayList<>(collection);
 		Collections.sort(list);
 		return list;
    }

   
    /**
     * Returns a string representation of the graph.
     * This method takes time proportional to <em>E</em> + <em>V</em>.
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,  
     *    followed by the <em>V</em> adjacency lists
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        String NEWLINE = System.getProperty("line.separator");
        s.append(this.V() + " vertices, " + E + " edges " + NEWLINE);   
        
        for (Item v : this.vertices()) {
        	s.append(v + ": ");
        	s.append( " degree:").append(this.degree(v)).append(" ");
        	s.append(" from: { ");
            for (Item w : this.publishingNodesOf(v)) {
            	s.append(w + " ");
            }            
            s.append("} to { ");
            for (Item w : this.subscribedNodesOf(v)) {
            	s.append(w + ", ");
            }
            s.append("}").append(NEWLINE);           
        }
        
        s.append(E + " edges " + NEWLINE);
        int i = 1;       
        for (Edge<Item> e : this.edges) {
        	s.append(i++).append(" edge: ").append(e).append(NEWLINE);             	
        }
        return s.toString();
    }
    
    
   

}
