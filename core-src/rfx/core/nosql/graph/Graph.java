package rfx.core.nosql.graph;

import rfx.core.util.io.StdOut;

/**
 *  The <tt>Graph</tt> class represents an undirected graph of vertices
 *  with string names.
 *  It supports the following operations: add an edge, add a vertex,
 *  get all of the vertices, iterate over all of the neighbors adjacent
 *  to a vertex, is there a vertex, is there an edge between two vertices.
 *  Self-loops are permitted; parallel edges are discarded.
 *  <p>
 *  For additional documentation, see <a href="http://introcs.cs.princeton.edu/45graph">Section 4.5</a> of
 *  <i>Introduction to Programming in Java: An Interdisciplinary Approach</i> by Robert Sedgewick and Kevin Wayne.
 */
public class Graph {

    // symbol table: key = string vertex, value = set of neighboring vertices
    private ST<String, SET<String>> st;

    // number of edges
    private int E;

   /**
     * Create an empty graph with no vertices or edges.
     */
    public Graph() {
        st = new ST<String, SET<String>>();
    }


   /**
     * Number of vertices.
     */
    public int V() {
        return st.size();
    }

   /**
     * Number of edges.
     */
    public int E() {
        return E;
    }

   /**
     * Degree of this vertex.
     */
    public int degree(String v) {
        if (!st.contains(v)) throw new RuntimeException(v + " is not a vertex");
        else return st.get(v).size();
    }

   /**
     * Add edge v-w to this graph (if it is not already an edge)
     */
    public void addEdge(String v, String w) {
        if (!hasEdge(v, w)) E++;
        if (!hasVertex(v)) addVertex(v);
        if (!hasVertex(w)) addVertex(w);
        st.get(v).add(w);
        st.get(w).add(v);
    }

   /**
     * Add vertex v to this graph (if it is not already a vertex)
     */
    public void addVertex(String v) {
        if (!hasVertex(v)) st.put(v, new SET<String>());
    }


   /**
     * Return the set of vertices as an Iterable.
     */
    public Iterable<String> vertices() {
        return st;
    }

   /**
     * Return the set of neighbors of vertex v as in Iterable.
     */
    public Iterable<String> adjacentTo(String v) {
        // return empty set if vertex isn't in graph
        if (!hasVertex(v)) return new SET<String>();
        else               return st.get(v);
    }

   /**
     * Is v a vertex in this graph?
     */
    public boolean hasVertex(String v) {
        return st.contains(v);
    }

   /**
     * Is v-w an edge in this graph?
     */
    public boolean hasEdge(String v, String w) {
        if (!hasVertex(v)) return false;
        return st.get(v).contains(w);
    }

   /**
     * Return a string representation of the graph.
     */
    public String toString() {
        String s = "";
        for (String v : st) {
            s += v + ": ";
            for (String w : st.get(v)) {
                s += w + " ";
            }
            s += "\n";
        }
        return s;
    }
    
    static void testSample1(){
    	  Graph G = new Graph();
          G.addEdge("A", "B");
          G.addEdge("A", "C");
          G.addEdge("C", "D");
          G.addEdge("D", "E");
          G.addEdge("D", "G");
          G.addEdge("E", "G");
          G.addVertex("H");
       
          // print out graph
          StdOut.println(G);

          // print out graph again by iterating over vertices and edges
          for (String v : G.vertices()) {
              StdOut.print(v + ": ");
              StdOut.print( " degree:" + G.degree(v) + " ");
              for (String w : G.adjacentTo(v)) {
                  StdOut.print(w + " ");
              }
              StdOut.println();
          }
    }
    
    static void testTopologyGraph2(){
  	  	Graph g = new Graph();
  	  	String[] actorPool1 = new String[] {"a1", "a2", "a3"};
  	  	String[] actorPool2 = new String[] {"b1", "b2", "b3"};
  	  	String[] actorPool3 = new String[] {"c1", "c2", "c3"};
  	  	
  	  	for (String pubActor : actorPool1) {
  	  		for (String subActor : actorPool2) {
  	  			g.addEdge(pubActor, subActor);
  	  		}			
		}
  	  	for (String pubActor : actorPool2) {
  	  		for (String subActor : actorPool3) {
  	  			g.addEdge(pubActor, subActor);
  	  		}
		}
  	  	
  	  // print out graph again by iterating over vertices and edges
        for (String v : g.vertices()) {
            StdOut.print(v + ": ");
            StdOut.print( " degree:" + g.degree(v) + " ");
            for (String w : g.adjacentTo(v)) {
                StdOut.print(w + " ");
            }
            StdOut.println();
        }
    }

    public static void main(String[] args) {
    	testTopologyGraph2();
    }

}
