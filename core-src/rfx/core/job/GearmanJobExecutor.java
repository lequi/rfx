package rfx.core.job;



import java.util.Date;

import org.gearman.Gearman;
import org.gearman.GearmanClient;
import org.gearman.GearmanJobEvent;
import org.gearman.GearmanJobReturn;
import org.gearman.GearmanServer;
import org.json.simple.JSONObject;

import rfx.core.configs.GearmanJobServerConfigs;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;


public class GearmanJobExecutor {
	final static String tag = "GearmanJobExecutor";
	final static String JOB_NAME = "process_job";
	
	static final String gearmanJobClazz =  "JobAdminCampaignProcess";
	static final String gearmanJobfunction =  "finishCampaignRealTime";	
	
	/** The host address of the job server */
	public static final String ECHO_HOST ;
	/** The port number the job server is listening on */
	public static final int ECHO_PORT;
		
	
	static {
		GearmanJobServerConfigs configs = GearmanJobServerConfigs.load();
		ECHO_HOST = configs.getHost();
		ECHO_PORT = configs.getPort();
	}
	
	public static void executeJobCampaignUnLinkProcess(int campaignId){
		JSONObject paramsArgs = new JSONObject();		
		paramsArgs.put("campaignId", campaignId);
		paramsArgs.put("type", "campaign");
		paramsArgs.put("action", "pause");
		paramsArgs.put("actor", "storm");
		
		JSONObject params = new JSONObject();		
		params.put("class", "JobAdminProcess");
		params.put("function", "unLinkProcess");
		params.put("args", paramsArgs);
		
		try {
			Gearman gearman = Gearman.createGearman();		
			GearmanClient client = gearman.createGearmanClient();	
			GearmanServer server = gearman.createGearmanServer(ECHO_HOST, ECHO_PORT);		
			client.addServer(server);			
			
			GearmanJobReturn jobReturn = client.submitJob(JOB_NAME, params.toJSONString().getBytes());

			while (!jobReturn.isEOF()) {				
				GearmanJobEvent event = jobReturn.poll();
				switch (event.getEventType()) {					
					case GEARMAN_JOB_SUCCESS: 		
						LogUtil.e(tag,"GEARMAN_JOB_SUCCESS.UnLinkCampaign: "+campaignId);
						break;	
					case GEARMAN_SUBMIT_FAIL: 
						LogUtil.e(tag,"GEARMAN_SUBMIT_FAIL");
					case GEARMAN_JOB_FAIL: 			
						LogUtil.e(tag,"GEARMAN_JOB_FAIL");
						LogUtil.e(tag, event.getEventType() + ": "+ new String(event.getData()));					
					default:
				}
			}		
			gearman.shutdown();
		} catch (Exception e) {		
			LogUtil.e(tag,"GEARMAN_SUBMIT_FAIL " + e.toString());
		}
	}

	public static String executeJobCampaignFinishing(String paramsJsonStr){
		String rs = "";
		try {
			Gearman gearman = Gearman.createGearman();		
			GearmanClient client = gearman.createGearmanClient();	
			GearmanServer server = gearman.createGearmanServer(ECHO_HOST, ECHO_PORT);		
			client.addServer(server);			
			
			GearmanJobReturn jobReturn = client.submitJob(JOB_NAME, paramsJsonStr.getBytes());

			while (!jobReturn.isEOF()) {				
				GearmanJobEvent event = jobReturn.poll();
				switch (event.getEventType()) {					
					case GEARMAN_JOB_SUCCESS: 		
						LogUtil.i(tag,"GEARMAN_JOB_SUCCESS.Finishing: "+new String(event.getData()));
						rs = "GEARMAN_JOB_SUCCESS";
						break;	
					case GEARMAN_SUBMIT_FAIL:
						rs = "GEARMAN_SUBMIT_FAIL";
						LogUtil.e(tag,"GEARMAN_SUBMIT_FAIL");						
					case GEARMAN_JOB_FAIL: 		
						rs = "GEARMAN_JOB_FAIL";
						LogUtil.e(tag,"GEARMAN_JOB_FAIL");
						LogUtil.e(tag, event.getEventType() + ": "+ new String(event.getData()));					
					default:
				}
			}		
			gearman.shutdown();
		} catch (Exception e) {		
			LogUtil.e(tag,"GEARMAN_SUBMIT_FAIL " + e.toString());
		}
		return rs;
	}
	
	public static String getGearmanParams(int compaignId, String type, String method){
		JSONObject paramsArgs = new JSONObject();
		paramsArgs.put("campaignId", compaignId);
		paramsArgs.put("method", method);// CPC | CPM
		paramsArgs.put("type", type  );//daily | total
		Date d = new Date();
		paramsArgs.put("date", DateTimeUtil.formatDate(d , "yyyy-MM-dd HH:mm:ss"));

		JSONObject params = new JSONObject();
		params.put("class", gearmanJobClazz);
		params.put("function", gearmanJobfunction);
		params.put("args", paramsArgs);		
		return params.toJSONString();
	}
	
	public static void notifyCpmmGearmanTask(int compaignId, String cpmRunningType, String params){		
								
		LogUtil.i("JobExecutor.notifyTask", params, true);
		String rs = GearmanJobExecutor.executeJobCampaignFinishing(params);
			
		
		
	}

	
}
