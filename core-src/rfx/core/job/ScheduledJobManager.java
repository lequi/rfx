
package rfx.core.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;

import rfx.core.configs.ScheduledJobConfig;



public class ScheduledJobManager {
	
	private static Collection<ScheduledJobConfig> scheduledJobsConfigs;
	
	List<ScheduledJob> autoTasks = new ArrayList<>();
	static ScheduledJobManager _instance;
	
	public static ScheduledJobManager getInstance(){
		if(_instance == null){
			_instance = new ScheduledJobManager();
		}
		return _instance;
	}
	
	protected ScheduledJobManager() {
		if(scheduledJobsConfigs == null){
			scheduledJobsConfigs = ScheduledJobConfig.getConfigs();	
			int size = scheduledJobsConfigs.size();
			System.out.println("AutoTasksScheduler started with "+ size + " tasks in queue");
			
		}
	}
	
	public int startScheduledJobs(int jobType){
		for (ScheduledJobConfig config : scheduledJobsConfigs) {
			if(config.getDelay() >= 0 && config.getJobType() == jobType){
				try {					
					Timer scheduledService = new Timer(true);//daemon process
					System.out.println("#process autoTaskConfig:"+config);
					Class clazz = Class.forName(config.getClasspath());
					ScheduledJob autoTask = (ScheduledJob) clazz.newInstance();
					autoTask.setScheduledJobsConfigs(config);
					
					if(config.getPeriod() == 0){
						scheduledService.schedule(autoTask, config.getDelay()*1000L);
					} else {
						scheduledService.schedule(autoTask, config.getDelay()*1000L, config.getPeriod()*1000L);	
					}
					autoTasks.add(autoTask);			
				}  catch (Exception e) {				
					e.printStackTrace();
				}
			}
		}
		return autoTasks.size();
	}
}
