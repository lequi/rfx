package rfx.core.cluster;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.net.util.Base64;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.configs.WorkerConfigs;
import rfx.core.model.KafkaTaskDef;
import rfx.core.model.TaskDef;
import rfx.core.model.WorkerData;
import rfx.core.model.WorkerInfo;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.HashUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;
import rfx.core.util.io.FileUtils;

import com.google.gson.Gson;

public class ClusterDataManager {
	static final String TAG = ClusterDataManager.class.getSimpleName();
	static final String CLUSTER_WORKER_PREFIX = "cluster-workers";

	//------------ configs ------------
	static final ClusterInfoConfigs clusterInfoConfigs = ClusterInfoConfigs.load();
	static final WorkerConfigs workerConfigs = WorkerConfigs.load();	
			
	static ShardedJedisPool commonClusterRedisPool = clusterInfoConfigs.getClusterInfoRedis().getShardedJedisPool();
	
	
	public static ShardedJedisPool getRedisClusterInfoPool() {
		return commonClusterRedisPool;
	}	
	
	public static long getUniqueRawIdForTopic(String topic) {
		long id = System.currentTimeMillis();

		ShardedJedisPool jedisPool = null;
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;
		try {
			jedisPool = getRedisClusterInfoPool();
			shardedJedis = getRedisClusterInfoPool().getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);			
			id = jedis.hincrBy("raw-log-ids", topic, 1L);
			isCommited = true;
		}
		catch (Exception e) {
			LogUtil.error(e);
		}
		finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
		return id;
	}	

	/////////////////// -----------deployment--------------- //////////////////////////////
	
	public static boolean deployJarFileFromBase64Data(final String base64Data, final String filepath){	
		byte[] decoded = Base64.decodeBase64(base64Data.getBytes());		
		FileUtils.writeStringToFile(filepath, decoded);
		return true;
	}
	
	public static boolean deployJarFile(final String key,final String crc32key,final String filepath){	
		ShardedJedisPool jedisPool =  getRedisClusterInfoPool();		
		boolean ok = (new RedisCommand<Boolean>(jedisPool) {
			@Override
			protected Boolean build() throws JedisException {
				boolean ok = false;
				String jarStream = jedis.hget(key, crc32key);
				if(StringUtil.isNotEmpty(jarStream) && filepath.endsWith("jar") ){
					String crc32 = HashUtil.crc32Number(jarStream)+"";
					if(crc32key.equals(crc32)){
						byte[] decoded = Base64.decodeBase64(jarStream.getBytes());					
						ok = FileUtils.writeStringToFile(filepath, decoded);
						jedis.hdel(key, crc32key);
					}				
				}		
				return ok;
			}
		}).execute();		
		return ok;
	}
	
	public static String updateJarFromFile(final String localFilepath){	
		ShardedJedisPool jedisPool =  getRedisClusterInfoPool();		
		String dk = (new RedisCommand<String>(jedisPool) {
			@Override
			protected String build() throws JedisException {
				StringBuilder s = new StringBuilder();
				try {
					String jarBase64Data = FileUtils.encodeFileToBase64Binary(localFilepath);
					
					if(jarBase64Data.length()>0){
						long crc32 = HashUtil.crc32Number(jarBase64Data);
						String dateStr = DateTimeUtil.getDateHourString(new Date());
						String dk = "deploy-"+dateStr;
						jedis.hset(dk, "" + crc32, jarBase64Data);
						jedis.expire(dk, 180);
						s.append(dk).append(" ").append(crc32);
					}					
				} catch (IOException e) {
					LogUtil.error(e);
				}
				return s.toString();
			}
		}).execute();	
		return dk;
	}
	
	public static String updateJarFromBase64Data(final String jarBase64Data){	
		ShardedJedisPool jedisPool =  getRedisClusterInfoPool();		
		String dk = (new RedisCommand<String>(jedisPool) {
			@Override
			protected String build() throws JedisException {
				StringBuilder s = new StringBuilder();
				if(jarBase64Data.length()>0){ 
					long crc32 = HashUtil.crc32Number(jarBase64Data);
					String dateStr = DateTimeUtil.getDateHourString(new Date());
					String dk = "deploy-"+dateStr;
					jedis.hset(dk, "" + crc32, jarBase64Data);
					jedis.expire(dk, 180);
					s.append(dk).append(" ").append(crc32);
				}
			
				return s.toString();
			}
		}).execute();	
		return dk;
	}
	
	/////////////////// -----------deployment--------------- //////////////////////////////
	
	public static void updateTopologyEvent(String topoName, String event){	
		boolean isCommited = false;
	
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();
		ShardedJedis shardedJedis = null;			
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);	
			Pipeline pipe = jedis.pipelined();		
			pipe.hincrBy(topoName, event, 1L);			
			pipe.sync();						
			isCommited = true;
		} catch (JedisException e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
	}
	
	public static void updateTopologyEvent(String topoName, String event, Object value){	
		boolean isCommited = false;
	
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();
		ShardedJedis shardedJedis = null;			
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);	
			Pipeline pipe = jedis.pipelined();		
			pipe.hset(topoName, event, value+"");			
			pipe.sync();						
			isCommited = true;
		} catch (JedisException e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
	}
	
		
	public static int getHourBackForSynchDataJob(String jobname){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;
		
		int hoursback = 2;
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);
			String h = jedis.hget("synch-hour-back",jobname);
			if(h != null){
				hoursback = StringUtil.safeParseInt(h);
				if(hoursback <= 0){
					jedis.hset("synch-hour-back", jobname, "2");
					hoursback = 2;
				}
			}				
			isCommited =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
		return hoursback;
	}
	
	public static int logSynchDataJobResult(String jobname, String value){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;
		
		int hoursback = 2;
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);			
			jedis.hset("synch-hour-back-logs", jobname, value);							
			isCommited =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
		return hoursback;
	}
	
	public static void logEvent(String eventName, String data){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;		
		
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);
			jedis.zadd(DateTimeUtil.getFormatedDateForLog(), DateTimeUtil.currentUnixTimestamp(), eventName + " " +data)	;
			isCommited =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}		
	}
	
	public static void logMetrics(Map<String, AtomicLong> counters){
		if(counters == null){
			return;
		}
		if(counters.size() == 0){
			return;
		}		
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;	
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);
			
			Pipeline pipeline = jedis.pipelined();
			
			Set<String> metricKeys = counters.keySet();
			System.out.println("------ Save monitor-metrics ------");
			for (String metricKey : metricKeys) {
				String c = String.valueOf(counters.get(metricKey).get());
				System.out.println("\t"+metricKey+" : "+c);
				pipeline.hset("monitor-metrics", metricKey, c);
			}
			pipeline.sync();
			System.out.println("----------------------------------");			
			isCommited =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}		
	}	
		
	public static void asynchStartWorkers(List<String> workerNames){
		ExecutorService exService = Executors.newFixedThreadPool(5);
		for (String w : workerNames) {
			final String workerName = w;
			exService.execute(new Runnable() {			
				@Override
				public void run() {
					 String shellWorkerPath = workerConfigs.getStartWorkerScriptPath();
						try {						
						    if( ! StringUtil.isEmpty(shellWorkerPath)){
								File file = new File(shellWorkerPath);
								if(file.isFile()){	
								    String execPath = shellWorkerPath + " " + workerName;		
								    Runtime.getRuntime().exec(execPath);
								    System.out.println("Runtime.getRuntime().exec:" + execPath);
								} else {
									LogUtil.e("ClusterDataManager.startWorkerNode", "worker-configs.xml, the startWorkerScriptPath is NOT valid shellscript"+shellWorkerPath);
								}
						    }
						} catch (Exception e) {
							LogUtil.error(e);
						} 
				}
			});
		}
		exService.shutdown();
	}
	
	/**
	 * set worker data (worker itself will call this )
	 * 
	 * @param workerName
	 * @param workerData
	 */
	public static void updateWorkerData(String workerName, WorkerData workerData){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean isCommited = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);			
			jedis.hset(CLUSTER_WORKER_PREFIX, workerName + ".data", new Gson().toJson(workerData));
			//String kt = new Gson().toJson(workerData);
			isCommited =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, isCommited);
		}
	}
	
	/**
	 * get all (the method for Master node dashboard )
	 * 
	 * @return
	 */
	public static List<WorkerData> getWorkerData(){		
		List<WorkerData>  datas = null;		
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);			
			Map<String,String> map = jedis.hgetAll(CLUSTER_WORKER_PREFIX);
			Set<String> keys = map.keySet();
			datas = new ArrayList<>(keys.size()/2);
			
			System.out.println(map);
			
			for (String key : keys) {
//				if(key.endsWith(".data"))
				{
					String jsonData = map.get(key);
					String jsonInfo = map.get(key.replace(".data", ".info"));
					if(jsonData != null){
						WorkerData workerData = new Gson().fromJson(jsonData, WorkerData.class);
						WorkerInfo workerInfo = new Gson().fromJson(jsonInfo, WorkerInfo.class);
						workerData.setHostname(workerInfo.getHost()+":"+workerInfo.getPort());
						workerData.setStatus(workerInfo.isAlive()?"ALIVE":"DIED");
						datas.add(workerData);
//						System.out.println(jsonData);
//						System.out.println(jsonInfo);
					}
				} 
			}			
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}		
		return datas == null ? new ArrayList<WorkerData>(0) : datas;
	}
	
	
	
	/**
	 * set TaskDef of worker (worker itself will call this )
	 * 
	 * @param workerName
	 * @param workerData
	 */
	public static void updateTaskDefOfWorker(String workerName, TaskDef taskDef){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);			
			jedis.hset(CLUSTER_WORKER_PREFIX, workerName + ".task", new Gson().toJson(taskDef));
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}
	}
	
	/**
	 * get TaskDef of worker (worker itself will call this )
	 * 
	 * @param workerName
	 * @param workerData
	 */
	public static TaskDef getTaskDefOfWorker(String workerName, Class<KafkaTaskDef> clazz){				
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;	
		TaskDef taskDef = null;
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);	
			String json = jedis.hget(CLUSTER_WORKER_PREFIX, workerName + ".task");
			if(json != null) {
				taskDef = new Gson().fromJson(json, clazz);
			}
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}
		return taskDef;
	}
	
	/**
	 * get all
	 * 
	 * @return
	 */
	public static boolean clearWorkerDataAndTaskDef(String workerName){			
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Pipeline pipeline = shardedJedis.getShard(StringPool.BLANK).pipelined();
			pipeline.hdel(CLUSTER_WORKER_PREFIX, workerName + ".data");
			pipeline.hdel(CLUSTER_WORKER_PREFIX, workerName + ".task");
			pipeline.sync();
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}		
		return ok;
	}
	
	
	////////////////////// static methods for WorkerInfo //////////////////////
	
	/**
	 * 
	 * save WorkerInfo of specified worker, supervisor itself will call this
	 * 
	 * @return boolean
	 */
	public static boolean saveWorkerInfo(WorkerInfo workerInfo){			
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Pipeline pipeline = shardedJedis.getShard(StringPool.BLANK).pipelined();
			pipeline.hset(CLUSTER_WORKER_PREFIX, workerInfo.getName() +".info", workerInfo.toJson());
			pipeline.sync();
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}		
		return ok;
	}
	
	/**
	 * 
	 * save WorkerInfo of specified worker, supervisor itself will call this
	 * 
	 * @return boolean
	 */
	public static boolean saveWorkerInfo(List<WorkerInfo> workerInfos){			
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;		
		try {				
			shardedJedis = jedisPool.getResource();
			Pipeline pipeline = shardedJedis.getShard(StringPool.BLANK).pipelined();
			for (WorkerInfo workerInfo : workerInfos) {
				pipeline.hset(CLUSTER_WORKER_PREFIX, workerInfo.getName() +".info", workerInfo.toJson());	
			}
			pipeline.sync();
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}		
		return ok;
	}
	
	/**
	 * registerWorkerInfo
	 * 
	 * @return boolean
	 */
	public static Map<String, WorkerInfo> getWorkerInfoFromRedis(){			
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;
		Map<String, WorkerInfo> mapWorkerInfo = null;
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);
			Map<String,String> map = jedis.hgetAll(CLUSTER_WORKER_PREFIX);
			Set<String> names = map.keySet();
			mapWorkerInfo = new HashMap<>(names.size());
			for (String name : names) {
				if(name.endsWith(".info")){
					String json = map.get(name);
					System.out.println(name + " json: " + json);
					if(StringUtil.isNotEmpty(json)){
						name = name.replace(".info", "");
						mapWorkerInfo.put(name, WorkerInfo.fromJson(json));					
					}
				}
			}
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}
		return mapWorkerInfo == null ? new HashMap<String, WorkerInfo>() : mapWorkerInfo;
	}
	
	/**
	 * registerWorkerInfo
	 * 
	 * @return boolean
	 */
	public static WorkerInfo getWorkerInfo(String name){			
		ShardedJedisPool jedisPool = getRedisClusterInfoPool();				
		ShardedJedis shardedJedis = null;
		boolean ok = false;
		WorkerInfo workerInfo = null;
		try {				
			shardedJedis = jedisPool.getResource();
			Jedis jedis = shardedJedis.getShard(StringPool.BLANK);
			String json = jedis.hget(CLUSTER_WORKER_PREFIX, name+".info");			
			if(StringUtil.isNotEmpty(json)){
				workerInfo = WorkerInfo.fromJson(json);
			}			
			ok =  true;
		} catch (Exception e) {
			LogUtil.error(e);
		} finally {
			RedisCommand.freeRedisResource(jedisPool, shardedJedis, ok);
		}		
		return workerInfo;
	}
	
	
}
