package rfx.core.cluster.node;

import rfx.core.exception.InvalidSetTaskDef;
import rfx.core.model.TaskDef;


/**
 * subscribe to Redis channel to get message, then emit to thread pool
 * 
 * @author trieu
 *
 */
public class FunctorNode extends WorkerNode {
	static final String TYPENAME = "Functor";
	public FunctorNode() {
		super();
		initEmptyWorker(TYPENAME);
	}
	
	public static FunctorNode getTheInstance() {
		return (FunctorNode) _instance;		
	}

	@Override
	public void startupWorkerNode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdownWorkerNode() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean isReadyAutoStartProcessingTask(){
		return false;
	}

	@Override
	public void start() {
		System.out.println("starting FunctorNode...");
	} 
	
	@Override
	public void setTaskDef(TaskDef taskDef) throws InvalidSetTaskDef{
		// TODO Auto-generated method stub		
	}
	
	@Override
	public void resetTaskDef(TaskDef taskDef) throws InvalidSetTaskDef {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearTaskDef() {
		// TODO Auto-generated method stub
	}

	////////////////////// Factory methods ////////////////////
	
	public static void startWorker(){
		// build the topology of actors
		WorkerNode workerNode = new FunctorNode();
		
		// start thread for monitoring worker		
		SupervisorNode.updateWorkerData(workerNode, "",0, 0);
	}
	
}
