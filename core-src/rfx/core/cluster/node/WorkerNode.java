package rfx.core.cluster.node;

import java.util.Timer;
import java.util.TimerTask;

import rfx.core.cluster.ClusterDataManager;
import rfx.core.cluster.node.handler.WorkerActionHandler;
import rfx.core.configs.WorkerConfigs;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.exception.InvalidSetTaskDef;
import rfx.core.functor.CoordinatingFunctor;
import rfx.core.kafka.KafkaDataSeeder;
import rfx.core.model.TaskDef;
import rfx.core.model.WorkerInfo;
import rfx.core.util.ActorUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.WorkerUtil;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.kernel.Bootable;

import com.typesafe.config.Config;

/**
 * abstract base class for workers {ScheduledJobWorker, LogParsingWorker}
 * 
 * @author trieu
 *
 */
public abstract class WorkerNode implements Bootable {

	protected static WorkerConfigs workerConfigs = WorkerConfigs.load();
	
	protected static WorkerNode _instance;
	protected ActorSystem actorSystem;
	protected static WorkerInfo workerInfo = null;//must static 
	protected TaskDef taskDef;

	
	protected WorkerNode() {
		ConfigAutoLoader.loadAll();
		checkSystem();
	}		

	
	/**
	 * init worker by specified name, called when the worker is already assigned tasks
	 * 
	 * @param workerName
	 */
	protected void initWorkerByName(String workerName){
		if(workerInfo != null){
			WorkerUtil.autoSystemExit("ERROR: initWorkerByName is already called", 0);
		}
		try {						
			workerInfo = ClusterDataManager.getWorkerInfo(workerName);
			
			System.out.println("getWorkerResource OK, workerName: "+workerName + " TCP Info:"+ workerInfo.getHost()+":"+workerInfo.getPort());
												
			Config config = ActorUtil.getWorkerNodeConfig(workerInfo).getConfig(workerName);
			System.out.println("config OK: "+config);
			
			actorSystem = ActorSystem.create("WorkerNode", config);
			Props props = Props.create(CoordinatingFunctor.class, WorkerActionHandler.getWorkerActions());
			ActorRef actor = actorSystem.actorOf(props, workerName);
			if(actor != null){
				LogUtil.i("--Created AnalyticWorkerNode at " + actor.path());
				workerInfo.setAlive(true);		
				ClusterDataManager.saveWorkerInfo(workerInfo);				
				
				//check if taskDef is defined, then auto starting system
				if(isReadyAutoStartProcessingTask()){
					start();
				}
			} else {
				WorkerUtil.autoSystemExit("ERROR: WorkerNode is shutdown, reason: Can not create CoordinatingActor for "+workerName, 100);
			}
		} catch (Exception e) {			
			LogUtil.error(e);
			WorkerUtil.autoSystemExit("ERROR: WorkerNode is shutdown, reason: "+e.getMessage(), 0);
		} finally {
			_instance = this;
		}
	}
	
	/**
	 * init empty worker, ready for new task
	 * 
	 * @param typeName
	 */
	protected void initEmptyWorker(String typeName){
		try {						
			workerInfo = ActorUtil.allocateWorkerInfo(typeName);
			String workerName = workerInfo.getName();
			
			System.out.println(typeName+ " started OK, name: "+workerName + " TCP Info:"+ workerInfo.getHost()+":"+workerInfo.getPort());
												
			Config config = ActorUtil.getWorkerNodeConfig(workerInfo).getConfig(workerName);
			System.out.println("config OK: "+config);
			
			actorSystem = ActorSystem.create("WorkerNode", config);
			Props props = Props.create(CoordinatingFunctor.class, WorkerActionHandler.getWorkerActions());
			ActorRef actor = actorSystem.actorOf(props, workerName);
			if(actor != null){
				LogUtil.i("--Created AnalyticWorkerNode at " + actor.path());
				workerInfo.setAlive(true);
				ClusterDataManager.saveWorkerInfo(workerInfo);				
				
				//check if taskDef is defined, then auto starting system
				if(isReadyAutoStartProcessingTask()){
					start();
				}
			} else {
				WorkerUtil.autoSystemExit("ERROR: WorkerNode is shutdown, reason: Can not create CoordinatingActor for "+workerName, 100);
			}
		} catch (Exception e) {			
			LogUtil.error(e);
			WorkerUtil.autoSystemExit("ERROR: WorkerNode is shutdown, reason: "+e.getMessage(), 0);
		} finally {
			_instance = this;
		}
	}
	
	/**
	 * @return String (the unique name of worker in system)
	 */
	public static String getWorkerName(){
		if(workerInfo != null){
			return workerInfo.getName();
		}
		return "";
	}
	
	public static WorkerInfo getWorkerInfo() {
		if(workerInfo != null){
			return workerInfo;
		}
		throw new IllegalAccessError("workerInfo is NULL, must call init worker first");
	}

	public static WorkerNode getTheInstance() {
		return _instance;
	}	
	
	protected void checkSystem(){
		if (_instance != null) {
			throw new IllegalArgumentException("Duplicated instance of AnalyticWorkerNode[" + workerInfo.getName() + "] in JVM Runtime");
		}
	}

	@Override
	public void shutdown() {
		Timer timer = new Timer();
		long delay = 10000;
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				WorkerUtil.autoSystemExit("Force shutdown after 10 seconds", 0);
			}
		}, delay);
		shutdownWorkerNode();
		actorSystem.shutdown();
		KafkaDataSeeder.shutdownMapDB();
		WorkerUtil.autoSystemExit("Shutdown worker ...", 0);
		timer.cancel();
	}

	@Override
	public void startup() {
		startupWorkerNode();
	}
	
	abstract public void startupWorkerNode();
	abstract public void shutdownWorkerNode();
	
	/**
	 * @return true if taskDef is not NULL
	 */
	abstract public boolean isReadyAutoStartProcessingTask();
	
	/**
	 * start processing or doing task.
	 * This method is called after call setTaskDef or resetTaskDef
	 */
	abstract public void start();
	
	/**
	 * set task definition for worker (the master will assign task at runtime)
	 * 
	 * @param taskDef
	 */
	abstract public void setTaskDef(TaskDef taskDef) throws InvalidSetTaskDef;
	
	/**
	 * clear task definition of worker and reset with new taskDef
	 * 
	 * @param taskDef
	 */
	abstract public void resetTaskDef(TaskDef taskDef) throws InvalidSetTaskDef;
	
	/**
	 * clear task definition of worker, the worker should be free after called 
	 */
	abstract public void clearTaskDef();

}