package rfx.core.cluster.node.handler;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import rfx.core.listener.RedisSubcribeListener;

@Deprecated
public class WebSocketHandler extends SimpleChannelHandler {

	
	static RedisSubcribeListener chanelManager = new RedisSubcribeListener(new DefaultChannelGroup());

	public static void closeAllChannels() {
		chanelManager.getAllChannels().close().awaitUninterruptibly();
	}
	
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e)
			throws Exception {		
		super.channelConnected(ctx, e);
	}

	@Override
	public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) {
		// Add all open channels to the global group so that they are closed on shutdown.
		chanelManager.addChannel(e.getChannel());		
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
			throws Exception {
		Object obj = e.getMessage();
		//System.out.println(obj.getClass());
		
		if(obj instanceof HttpRequest){
			//skip
			System.out.println();
		} 
		else if (obj instanceof TextWebSocketFrame) {
			TextWebSocketFrame frame = (TextWebSocketFrame) obj;
			//System.out.println("got at server "+frame.getText());			
			ctx.getChannel().write(new TextWebSocketFrame(frame.getText().toUpperCase()));
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
			throws Exception {
		e.getCause().printStackTrace();
		e.getChannel().close();
	}

}