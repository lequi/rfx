package rfx.core.cluster.node.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.netty.handler.codec.http.QueryStringDecoder;

import rfx.core.configs.KafkaTopologyConfig;
import rfx.core.util.HttpResponseUtil;
import rfx.core.util.StringUtil;
import rfx.core.util.SupervisorUtil;

@Deprecated
public class HttpMasterAjaxHandler {
	public static Map<String, Object> ajaxHandler(String uri)
	{
		Map<String, Object> responseData = new HashMap<String, Object>(2);

		QueryStringDecoder queryStringDecoder = new QueryStringDecoder(uri);
		Map<String, List<String>> parameter = queryStringDecoder.getParameters();
		String action = HttpResponseUtil.getParamValue("action", parameter);
		String workerName = HttpResponseUtil.getParamValue("worker_name",parameter);
		String hostname = HttpResponseUtil.getParamValue("hostname",parameter);
		hostname = StringUtil.replace(hostname, "%3A", ":");
		String[] hostInfoArr = hostname.split(":");

		System.out.println("ajaxHandler "+ action);
		switch (action) {
			case "kill_all":
        			SupervisorUtil.killAllWorkers(true);
        			responseData.put("status", "ok");
        			responseData.put("msg", "success");
        			break;
			case "kill":
				SupervisorUtil.kill(workerName);
				responseData.put("status", "ok");
				responseData.put("msg", "success");
				break;
	
			case "start":
				String kafkaTopic = HttpResponseUtil.getParamValue(
						"kafka_topic", parameter);
				String rangePartiotion = HttpResponseUtil.getParamValue(
						"range_partition", parameter);
				String[] rangePartiotionArr = rangePartiotion.split("-");
	
				int beginPartition = 0;
				int endPartition = 0;
				if (hostInfoArr.length >= 2) {
					beginPartition = StringUtil.safeParseInt(
							rangePartiotionArr[0], 0);
					endPartition = StringUtil.safeParseInt(
							rangePartiotionArr[1], 1);
				}
	
				// SupervisorNode.startNode(workerName, kafkaTopic,
				// beginPartition, endPartition);
				responseData.put("status", "ok");
				responseData.put("msg", "success");
				break;
	
			case "check":
				boolean kt = SupervisorUtil.checkWorkerStatus(workerName);
				if (kt) {
					responseData.put("status", "ok");
					responseData.put("msg", "alive");
				} else {
					responseData.put("status", "ok");
					responseData.put("msg", "dead");
				}
				break;
			
			case "get_offset":
			    kafkaTopic = HttpResponseUtil.getParamValue("kafka_topic", parameter);
			    Map<String, Object> kafkaOffset = SupervisorUtil.getKafkaOffsetAtWorker(workerName, kafkaTopic);
			    if(kafkaOffset != null && kafkaOffset.size() > 0){
				    
				  String data = HttpResponseUtil.responseAsJsonp("", kafkaOffset);
				  responseData.put("status", "ok");
				  responseData.put("msg",  data);
			    }
			    else {
				responseData.put("status", "ok");
				responseData.put("msg", "{}");
			    }
			    
			    break;
			case "set_offset":
			    
			    kafkaTopic = HttpResponseUtil.getParamValue(
					"kafka_topic", parameter);
			    beginPartition = StringUtil.safeParseInt(HttpResponseUtil.getParamValue(
					"begin_partition", parameter));
			    endPartition = StringUtil.safeParseInt(HttpResponseUtil.getParamValue(
					"end_partition", parameter));
			    String dataOffset = HttpResponseUtil.getParamValue(
					"data_offset", parameter);
			    
			    String[] dataOffsets = dataOffset.split(",");
			    StringBuilder sb = new StringBuilder();
			    
        		    if (dataOffsets.length >= 0) {
        			//String key;
        			for(int i=0; i< dataOffsets.length; i++){
        			    String[] partItems = dataOffsets[i].split(":");
        			    if(partItems.length == 2){
        				
        				String partitionKey = StringUtil.safeString(partItems[0]);
        				Long offset = StringUtil.safeParseLong(partItems[1], 0);
        				
        				sb.append("," + partitionKey + "=" + offset);
        			    }
        			}
        		    }
        		    
        		    String partitionOffset = sb.toString();
        		    if(partitionOffset != null && partitionOffset.length() > 0){
        			  partitionOffset = partitionOffset.substring(1);
        			  
        			  SupervisorUtil.setKafkaOffset(workerName, kafkaTopic, partitionOffset);
        			  responseData.put("status", "ok");
      			    	  responseData.put("msg", "success");
        		    }
        		    else{
        			  responseData.put("status", "fail");
      			    	  responseData.put("msg", "offset post null");
        		    }
        		  
			    break;
			case "get_kafka_topic":			    
			    List<String> kafkaTopics = KafkaTopologyConfig.getKafkaTopic();
			    Map<String, Object> mapkafkaTopics = new HashMap<>(1);
			    mapkafkaTopics.put("kafkaTopics", (Object) kafkaTopics);
			    
			    String data = HttpResponseUtil.responseAsJsonp("", mapkafkaTopics);
			    responseData.put("status", "ok");
			    responseData.put("msg",  data);
			    
			    break;
			case "set_worker_topic":
			    
			    kafkaTopic = HttpResponseUtil.getParamValue("kafka_topic", parameter);
			    beginPartition = StringUtil.safeParseInt(HttpResponseUtil.getParamValue("begin_partition", parameter));
			    endPartition = StringUtil.safeParseInt(HttpResponseUtil.getParamValue("end_partition", parameter));
			    
			    int check = SupervisorUtil.setKafkaTopic(workerName, kafkaTopic, beginPartition, endPartition);
			    if(check == 0){
					responseData.put("status", "ok");
					responseData.put("msg",  "success");
			    }
			    else if(check == 1){
					responseData.put("status", "fail");
					responseData.put("msg",  "connect to worker is refured");
			    }
			    else{
					responseData.put("status", "fail");
					responseData.put("msg",  "timeout to send execute to worker");
			    }
			    
			    break;
			default:
				break;
		}
		return responseData;
	}
}
