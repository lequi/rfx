package rfx.core.cluster.node;

public abstract class BaseWorker {
	
	static BaseWorker _worker;
	
	public static BaseWorker getInstance(){
		return _worker;
	}
	
	protected static void setWorker(BaseWorker worker){
		_worker = worker;
	}
	

	String name;	
	
	public BaseWorker(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	

	abstract protected void initWorker();
	
	@Override
	public String toString() {		
		return name == null ? BaseWorker.class.getSimpleName() : name;
	}
}
