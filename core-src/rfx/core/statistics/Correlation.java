package rfx.core.statistics;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class Correlation {
	public static double getPearsonCorrelation(double[] scores1, double[] scores2) {
		double result = 0;
		double sum_sq_x = 0;
		double sum_sq_y = 0;
		double sum_coproduct = 0;
		double mean_x = scores1[0];
		double mean_y = scores2[0];
		for (int i = 2; i < scores1.length + 1; i += 1) {
			double sweep = Double.valueOf(i - 1) / i;
			double delta_x = scores1[i - 1] - mean_x;
			double delta_y = scores2[i - 1] - mean_y;
			sum_sq_x += delta_x * delta_x * sweep;
			sum_sq_y += delta_y * delta_y * sweep;
			sum_coproduct += delta_x * delta_y * sweep;
			mean_x += delta_x / i;
			mean_y += delta_y / i;
		}
		double pop_sd_x = (double) Math.sqrt(sum_sq_x / scores1.length);
		double pop_sd_y = (double) Math.sqrt(sum_sq_y / scores1.length);
		double cov_x_y = sum_coproduct / scores1.length;
		result = cov_x_y / (pop_sd_x * pop_sd_y);
		return result;
	}
	
	static void testPearsonCorrelation(){
		double[] scores1 = new double[] {1, 3, 4, 4};
		double[] scores2 = new double[] {2, 5, 5, 8};
		
		Stopwatch stopwatch = new Stopwatch().start();
		double score = 0;
		for (int i = 0; i < 100000; i++) {
			score = getPearsonCorrelation(scores1, scores2);			
		}
		System.out.println(score);	
		System.out.println("totalTime in milliseconds is "+stopwatch.elapsedTime(TimeUnit.MILLISECONDS));
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 3; i++) {
			testPearsonCorrelation();			
		}		
	}
	
	
}
