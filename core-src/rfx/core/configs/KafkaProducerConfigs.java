package rfx.core.configs;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import rfx.core.kafka.producer.SimplePartitioner;
import rfx.core.util.CommonUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class KafkaProducerConfigs implements Serializable {
	
	
	static KafkaProducerConfigs _instance;

	private static final long serialVersionUID = 4936959262031389418L;
	
	
	
	private int debugModeEnabled = 0;	
	int numberBatchJob = 20;
	int timeSendKafkaPerBatchJob = 4000;
	int sendKafkaThreadPerBatchJob = 3;
	int sendKafkaMaxRetries = 100000;
	int kafkaProducerAsyncEnabled = 1;
	int kafkaProducerAckEnabled = 1;
	private String defaultPartitioner = SimplePartitioner.class.getName();
	Map<String,Map<String,String>> kafkaProducerList;
	
	public int getKafkaProducerAsyncEnabled() {
		return kafkaProducerAsyncEnabled;
	}

	public void setKafkaProducerAsyncEnabled(int kafkaProducerAsyncEnabled) {
		this.kafkaProducerAsyncEnabled = kafkaProducerAsyncEnabled;
	}

	public int getKafkaProducerAckEnabled() {
		return kafkaProducerAckEnabled;
	}

	public void setKafkaProducerAckEnabled(int kafkaProducerAckEnabled) {
		this.kafkaProducerAckEnabled = kafkaProducerAckEnabled;
	}

	public int getSendKafkaMaxRetries() {
		return sendKafkaMaxRetries;
	}

	public void setSendKafkaMaxRetries(int sendKafkaMaxRetries) {
		this.sendKafkaMaxRetries = sendKafkaMaxRetries;
	}

	public int getNumberBatchJob() {
		return numberBatchJob;
	}

	public void setNumberBatchJob(int numberBatchJob) {
		this.numberBatchJob = numberBatchJob;
	}

	

	public int getDebugModeEnabled() {
		return debugModeEnabled;
	}

	public void setDebugModeEnabled(int debugModeEnabled) {
		this.debugModeEnabled = debugModeEnabled;
	}

	public KafkaProducerConfigs() {
		super();
	}
	
	

	public Map<String, Map<String, String>> getKafkaProducerList() {
		if(kafkaProducerList == null){
			kafkaProducerList = new HashMap<>();
		}
		return kafkaProducerList;
	}

	public void setKafkaProducerList(
			Map<String, Map<String, String>> kafkaProducerList) {
		this.kafkaProducerList = kafkaProducerList;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();

		return s.toString();
	}
	
	
	public static final KafkaProducerConfigs load(String configPath) {
		if (_instance == null) {
			try {
				String json = FileUtils.readFileAsString(configPath);
				_instance = new Gson().fromJson(json, KafkaProducerConfigs.class);
				LogUtil.i("HttpServerConfigs loaded and create new instance from "+ CommonUtil.getKafkaProducerConfigFile());
			} catch (Exception e) {
				if (e instanceof JsonSyntaxException) {
					e.printStackTrace();
					System.err.println("Wrong JSON syntax in file "+CommonUtil.getKafkaProducerConfigFile());
				} else {
					e.printStackTrace();
				}
			}
		}
		return _instance;
	}
	
	public static final KafkaProducerConfigs load() {
		return load(CommonUtil.getKafkaProducerConfigFile());
	}

	
	public String getDefaultPartitioner() {
		return defaultPartitioner;
	}

	
	public int getSendKafkaThreadPerBatchJob() {
		return sendKafkaThreadPerBatchJob;
	}

	public void setSendKafkaThreadPerBatchJob(int sendKafkaThreadPerBatchJob) {
		this.sendKafkaThreadPerBatchJob = sendKafkaThreadPerBatchJob;
	}

	public int getTimeSendKafkaPerBatchJob() {
		return timeSendKafkaPerBatchJob;
	}

	public void setTimeSendKafkaPerBatchJob(int timeSendKafkaPerBatchJob) {
		this.timeSendKafkaPerBatchJob = timeSendKafkaPerBatchJob;
	}
	
}