package rfx.core.configs;

import java.io.Serializable;

import com.google.gson.Gson;


public class GearmanJobServerConfigs implements Serializable{

	private static final long serialVersionUID = -3769130766370675109L;

	static GearmanJobServerConfigs _instance;
		
	String host;
	int port;	
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public static final GearmanJobServerConfigs load() {		
		return _instance;
	}
	
	public String toJson(){
		return new Gson().toJson(this);
	}

}
