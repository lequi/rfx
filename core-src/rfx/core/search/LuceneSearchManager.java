package rfx.core.search;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortField.Type;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import rfx.core.model.crawler.Item;
import rfx.core.util.StringUtil;

public class LuceneSearchManager {
	
	static Version curVersion = Version.LUCENE_47; 

	// To store an index on disk
	Directory directory;
	StandardAnalyzer analyzer = new StandardAnalyzer(curVersion);
	
	static String luceneIndexFilePath = "data/lucene-index";
	
	static LuceneSearchManager _instance;
	public static LuceneSearchManager get(){
		if(_instance == null){
			try {
				_instance = new LuceneSearchManager();
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		return _instance;
	}
	
	void initDirectory() throws IOException{
		boolean create = false;
		File indexDirFile = new File(luceneIndexFilePath);
		if (!indexDirFile.exists() || !indexDirFile.isDirectory()) {
			create = indexDirFile.mkdir();
		} else {
			create = true;
		}
		if( ! create ){
			throw new IllegalArgumentException("Can not create the index lucene at path "+luceneIndexFilePath);
		}		
		directory = FSDirectory.open(indexDirFile);		
	}

	public LuceneSearchManager() throws IOException {
		initDirectory();
	}
	
	static Document fromItemToDoc(Item item) {
		Document doc = new Document();
		doc.add(new TextField("name", item.getName(), Field.Store.YES));
		doc.add(new StringField("url", item.getUrl(), Field.Store.YES));
		doc.add(new StringField("imageUrl", item.getImageUrl(), Field.Store.YES));
		doc.add(new IntField("totalSocialScore", item.getSocialMetric().getTotalSocialScore(), Field.Store.YES));
		doc.add(new TextField("contextLocation", item.getContext().getLocation(), Field.Store.YES));
		return doc;
	}
	
	static Item fromDocToItem(Document doc) {
		Item item = new Item(doc.get("url"), doc.get("name"));
		item.setTotalSocialScore(StringUtil.safeParseInt(doc.get("totalSocialScore")));
		item.setContextLocation(doc.get("contextLocation"));
		return item;
	}
	
	
	public void indexAndStoreItems(Item ... items){
		IndexWriter w;
		try {
			IndexWriterConfig config = new IndexWriterConfig(curVersion, analyzer);
			w = new IndexWriter(directory, config);
			for (Item item : items) {
				w.addDocument(fromItemToDoc(item));
			}
			w.commit();	
			w.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {}
	}
	
	public void indexAndStoreItems(List<Item> items){
		indexAndStoreItems((Item[]) items.toArray());
	}
	
	public List<Item> search(String keywords, String contextLocation){
		List<Item> items = new ArrayList<>(12);
		try {
			int maxLimit = 12;
			String[] searchableFields = new String[] {"name","contextLocation"};
			MultiFieldQueryParser parser = new MultiFieldQueryParser(curVersion, searchableFields , analyzer);
			Query query = parser.parse(keywords+" "+contextLocation);
			
			IndexReader reader = DirectoryReader.open(directory);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(query, maxLimit, new Sort(new SortField("totalSocialScore", SortField.Type.INT, true)));
			ScoreDoc[] scoreDocList = docs.scoreDocs;
			
			System.out.println("Found " + scoreDocList.length + " hits.");
			for (int i = 0; i < scoreDocList.length; ++i) {
				int docId = scoreDocList[i].doc;
				Document doc = searcher.doc(docId);
				items.add(fromDocToItem(doc));
			}

			// reader can only be closed when there, no need to access the documents any more.
			reader.close();
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return items;
	}
	
	public List<Item> search2(String keywords, String contextLocation){
		// the "title" arg specifies the default field to use
		// when no field is explicitly specified in the query.
		List<Item> items = new ArrayList<>(12);
		try {
			BooleanQuery booleanQuery = new BooleanQuery();			
			Query query1 = new TermQuery(new Term("name", keywords));
			Query query2 = new TermQuery(new Term("locationAddress", contextLocation));
			booleanQuery.add(query1, BooleanClause.Occur.SHOULD);
			booleanQuery.add(query2, BooleanClause.Occur.SHOULD);
			
			Sort socialSort = new Sort(new SortField("totalSocialScore", Type.INT ), SortField.FIELD_SCORE);

			// 3. search
			int hitsPerPage = 12;
			IndexReader reader = DirectoryReader.open(directory);
			IndexSearcher searcher = new IndexSearcher(reader);
			
			
			TopDocs docs = searcher.search(booleanQuery, hitsPerPage, socialSort);
			ScoreDoc[] scoreDocList = docs.scoreDocs;

			// 4. display results
			System.out.println("Found " + scoreDocList.length + " hits.");
			for (int i = 0; i < scoreDocList.length; ++i) {
				int docId = scoreDocList[i].doc;
				Document doc = searcher.doc(docId);
				items.add(fromDocToItem(doc));
			}

			// reader can only be closed when there, no need to access the documents any more.
			reader.close();
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return items;
	}
	
	public void close(){
		try {
			if(directory != null){
				directory.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
//TODO
// http://develop.nydi.ch/2010/10/24/lucene-spatial-example/
