package rfx.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.io.CmdProcessUtil;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class RfxRecommendationUtil {

	static boolean noCache = false;
    static ShardedJedisPool jedisPool = ClusterInfoConfigs.load().getClusterInfoRedis().getShardedJedisPool();
    static final String GOOGLE_BASE_QUERY = "https://www.google.com.vn/search?q=";

    static String buildQueryCmd(String query) {
        StringBuilder cmd = new StringBuilder();
        cmd.append("/home/trieu/Programs/phantomjs-1.9.7/bin/phantomjs").append(StringPool.SPACE);
        cmd.append("/home/trieu/git-projects/rfx-src/rfx/src-script/crawler/test-get-links.js").append(StringPool.SPACE);
        cmd.append(GOOGLE_BASE_QUERY).append(StringUtil.encodeUrlUTF8(query));
        cmd.append("&&num=20");
        return cmd.toString();
    }
    //http://www.latlong.net/Show-Latitude-Longitude.html

    static Map<String, List<String>> users_SortedLocations = new HashMap<>();
    static Map<String, List<String>> users_SortedDomains = new HashMap<>();
    static Map<String, List<String>> users_InterestedTopics = new HashMap<>();

    static {
        List<String> sites = new ArrayList<String>();
        sites.add("diadiemanuong.com");
        users_InterestedTopics.put("675354505", sites);
    }

    public static String suggestMe(String userId, String keywords, String context) {
        // System.out.println("#### suggestMe q:" + q);
        String site = "diadiemanuong.com";

        StringBuilder rs = new StringBuilder();
        try {
            String query = "quán ăn \"Quận 7\" \"Q7\" \"Quan 7\" Ho Chi Minh, Vietnam ";
            String html = askGoogle(buildQueryCmd(query));
            Document doc = Jsoup.parse(html);
            Elements elements = doc.select("a");
            for (Element element : elements) {
                String href = element.attr("href");
                href = href.substring(href.indexOf("http://"));
                int lastIndex = href.lastIndexOf("&sa=");
                if (lastIndex > 0 && !href.contains("site:")) {
                    href = href.substring(0, lastIndex);
                    int like = SocialAnalyticsUtil.getTotalCountFacebookStats(href);
                    Document itemDoc = Jsoup.parse(getHtmlFromUrl(href));
                    System.out.println("title: " + itemDoc.title() + " url:" + href + " \n");
                    String imageUrl = itemDoc.select("div[class=thumb]").select("img").get(0).attr("src");
                    String name = itemDoc.title();

                    rs.append("<a href='").append(href).append("' target='_blank'>");
                    rs.append("<img class='item_img' src='").append(imageUrl).append("' />");
                    rs.append("<span class='item_name'>").append(name).append("</span>");
                    rs.append("</a>");
                    rs.append("<b> Facebook Score: ").append(like).append("</b>");
                    rs.append("<br>");
                   
                }
            }
        } catch (Exception e) {
        }
        return rs.toString();
    }

    static void testWithRx() {
        try {
            String query = "quán ăn \"Quận 6\" \"Q6\" \"Quan 6\" Ho Chi Minh, Vietnam site:diadiemanuong.com";
            String html = CmdProcessUtil.exec(buildQueryCmd(query));
            Document doc = Jsoup.parse(html);
            Elements elements = doc.select("a");
            Observable.from(elements).map(new Func1<Element, String>() {
                @Override
                public String call(Element element) {
                    String href = element.attr("href");
                    href = href.substring(href.indexOf("http://diadiemanuong.com"));
                    int lastIndex = href.lastIndexOf("&sa=");
                    if (lastIndex > 0 && href.indexOf("site:") == -1) {
                        href = href.substring(0, lastIndex);
                        return href;
                    }
                    return null;
                }
            }).filter(new Func1<String, Boolean>() {
                @Override
                public Boolean call(String href) {
                    return href != null;
                }
            }).subscribe(new Action1<String>() {
                @Override
                public void call(String href) {
                    Document itemDoc = Jsoup.parse(HttpClientUtil.executeGet(href));
                    System.out.println("title: " + itemDoc.title() + " url:" + href + " \n");
                    Utils.sleep(200);
                }
            });

        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    static String getHtmlFromUrl(final String url){ 
    	System.out.println("### getHtmlFromUrl "+url);
    	if(noCache){
    		return HttpClientUtil.executeGet(url);
    	}
        String rs = (new RedisCommand<String>(jedisPool) {
            @Override
            public String build() throws JedisException {
                String hash = HashUtil.hashUrlCrc64(url) + "";
                String key = "url:" + hash;
                String rs = jedis.hget(key, "html");
                if (StringUtil.isEmpty(rs)) {
                	Utils.sleep(100);
                    rs = HttpClientUtil.executeGet(url);
                    jedis.hset(key, "html", rs);
                    jedis.hset(key, "url", url);
                }
                return rs;
            }
        }).execute();
        return rs;
    }

    static String askGoogle(final String q) {
    	System.out.println("### askGoogle "+q);
    	if(noCache){
    		return CmdProcessUtil.exec(q);
    	}
        String rs = (new RedisCommand<String>(jedisPool) {
            @Override
            public String build() throws JedisException {
                String hash = HashUtil.hashUrlCrc64(q) + "";
                String key = "q:" + hash;
                String rs = jedis.hget(key, "html");
                if (StringUtil.isEmpty(rs)) {
                    rs = CmdProcessUtil.exec(q);
                    jedis.hset(key, "html", rs);
                    jedis.hset(key, "q", q);
                }
                return rs;
            }
        }).execute();
        return rs;
    }

    static String addressToLocation(String address) {
        //from Quận 6, Ho Chi Minh City, Vietnam
        //to \"Quận 6\" \"Q6\" \"Quan 6\" Ho Chi Minh, Vietnam
        String[] toks = address.split(",");
        String sQ = toks[0];
        String raw = VietnameseTextUtil.removeAccent(toks[0]);
        try {
            sQ = sQ.charAt(0) + sQ.split(" ")[1];
        } catch (Exception e) {
        }

        StringBuilder s = new StringBuilder();
        s.append("\"").append(toks[0]).append("\"").append(" ");
        s.append("\"").append(sQ).append("\"").append(" ");
        s.append("\"").append(raw).append("\"").append(" ");
        s.append(toks[1]).append(",").append(toks[2]);

        return s.toString();
    }

    public static void main(String[] args) {
        System.out.println(addressToLocation("Quận 6, Ho Chi Minh City, Vietnam"));
    }
}
