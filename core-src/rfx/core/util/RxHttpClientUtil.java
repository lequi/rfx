package rfx.core.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;
import rx.functions.Action1;

import com.google.common.base.Stopwatch;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

public class RxHttpClientUtil {
	
	public static String httpGet(String url){
		String rs = "";
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		try {			
			rs =  asyncHttpClient.prepareGet(url).execute().get().getResponseBody();			
		} catch (Exception e) {}
		finally {
			asyncHttpClient.close();
		}
		return rs;
	}
	
	//static final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
	public static void asynchHttpGet(String url, final Action1<String> action){
		@SuppressWarnings("resource")
		final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		try {			
			asyncHttpClient.prepareGet(url).execute(new AsyncCompletionHandler<Response>(){
			    @Override
			    public Response onCompleted(Response response) throws Exception{			    	
			    	Observable.from(response.getResponseBody()).subscribe(action);
			    	asyncHttpClient.closeAsynchronously();
			        return response;
			    }

			    @Override
			    public void onThrowable(Throwable t){
			        // Something wrong happened.
			    	asyncHttpClient.closeAsynchronously();
			    }
			});
			
		} catch (Exception e) {}		
	}

	public static void main(String[] args) throws Exception {
				
		String url = "http://vnexpress.net/";
		//System.out.println(RxHttpClientUtil.httpGet(url).length());
		
		Stopwatch stopwatch = new Stopwatch().start();
		final AtomicInteger c = new AtomicInteger();
		int MAX = 200;
		for (int i = 0; i < MAX; i++) {
			//200 http call in SECONDS: 7566
			RxHttpClientUtil.asynchHttpGet(url, new Action1<String>() {
				@Override
				public void call(String data) {
					//System.out.println(data.length());
					if(data.length()>0){
						c.incrementAndGet();
					}
				}
			});
			
//			200 http call in SECONDS: 27299
//			String data = HttpClientUtil.executeGet(url);
//			if(data.length()>0){
//				c.incrementAndGet();
//			}
		}		
		while (c.get() < MAX) {}
		System.out.println(MAX+ " http call in SECONDS: "+stopwatch.elapsedTime(TimeUnit.MILLISECONDS));
		
		//Utils.sleep(10000);
		System.exit(1);
	
	}
}
