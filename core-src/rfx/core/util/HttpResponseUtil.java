package rfx.core.util;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;

import org.jboss.netty.channel.ChannelHandlerContext;

import com.google.gson.Gson;

public class HttpResponseUtil {

	
	static final String HEADER_REFERER_NAME = "Referer";
	static final String HEADER_REFRESH_NAME = "Refresh";
	static final String HEADER_LOCATION_NAME = "Location";	
	static final String HEADER_CONNECTION_CLOSE = "Close";
	
	//redirect to url using HTML+JavaScript to preserve the referer, solution at https://coderwall.com/p/7a09ja
	static final String HTML_FOR_REDIRECT;
	static {
		StringBuilder s= new StringBuilder();
		s.append("<!DOCTYPE html><html><head><title></title></head><body>");
		s.append("<script type='text/javascript' >window.location=\"$url\";</script>");
		//tracking when javascript can not redirect to targeted url
		//s.append("<noscript><img src='http://localhost:8080/ar?redirect=$autourl' /></noscript>");
		s.append("</body></html>");		
		HTML_FOR_REDIRECT = s.toString();
	}
	

	public static String getParamValue(String name, Map<String, List<String>> params) {
		return getParamValue(name, params, StringPool.BLANK);
	}
	
	public static String getParamValue(String name, Map<String, List<String>> params, String defaultVal) {
		List<String> vals = params.get(name);
		if (vals != null) {
			if (!vals.isEmpty()) {
				return vals.get(0);
			}
		}
		return defaultVal;
	}
	
	public static String getRemoteIP(ChannelHandlerContext ctx) {
		try {
			SocketAddress remoteAddress = ctx.getChannel().getRemoteAddress();
			if(remoteAddress instanceof InetSocketAddress){
				return ((InetSocketAddress)remoteAddress).getAddress().getHostAddress();
			}
			return remoteAddress.toString().split("/")[1].split(":")[0];
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "0.0.0.0";
	}
	
	public static boolean isBadLogRequest(String uri){
		if(StringUtil.isEmpty(uri)){
			return true;
		}
		int idx = uri.indexOf("?");
		if(idx < 0){
			return true;
		}
		String queryDetails = uri.substring(idx+1);
		if(StringUtil.isEmpty(queryDetails)){
			return true;
		}
		return false;
	}
	
	
	public static String responseAsJsonp(String callbackFunc, Map<String, Object> data){
		String jsonData = new Gson().toJson(data);
		if( StringUtil.isEmpty(callbackFunc) ){			
			return jsonData;
		} else {
			StringBuilder jsonp = new StringBuilder(callbackFunc);
			jsonp.append("(").append(jsonData).append(")");
			return jsonp.toString();
		}	
	}
	
}
