package rfx.core.util;

import java.io.File;

public class TestUtils {
	public static File createTempSubdir(String name){		
		return createTempSubdir(name, true);
	}
	
	public static File createTempSubdir(String name,boolean cleanExistedFiles){
		File d = new File("data/"+name);
		if( ! d.isDirectory() ){
			d.mkdir();
		} else {
			if(cleanExistedFiles){
				File[] files = d.listFiles();
				for (File file : files) {
					file.delete();
				}
			}
		}		
		return d;
	}
}
