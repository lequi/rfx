package rfx.core.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import rfx.core.util.HttpClientUtil;
import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Observer;
import rx.Subscriber;

public class HttpClientConcurrentCaller {
	private ExecutorService executor;	
	
	public HttpClientConcurrentCaller() {
		super();
		this.executor = new ThreadPoolExecutor(4, 4, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>());
	}

	protected static class CallWebServiceTask implements Callable<Void> {
		Observer<? super String> observer;
		String url;
				
		public CallWebServiceTask(Observer<? super String> observer, String url) {
			super();
			this.observer = observer;
			this.url = url;
		}

		@Override
		public Void call() throws Exception {			
			observer.onNext(HttpClientUtil.executeGet(url));
			return null;
		}
	};
	
	public Observable<String> call(final String url){
		return Observable.create(new OnSubscribe<String>() {
			@Override
			public void call(Subscriber<? super String> subscriber) {			
				if(subscriber.isUnsubscribed()){
					return;
				}
				try {
					executor.submit(new CallWebServiceTask(subscriber, url)).get();
				} catch (Exception e) {
					subscriber.onError(e);
				}			
				subscriber.onCompleted();	
				executor.shutdownNow();
				return;
			}
		});   
	}
	
	public Observable<String> call(final String ... urls){
		return Observable.create(new OnSubscribe<String>() {
			@Override
			public void call(Subscriber<? super String> subscriber) {
				for (String url : urls) {
					if(subscriber.isUnsubscribed()){
						return;
					}
					try {
						executor.submit(new CallWebServiceTask(subscriber, url)).get();
					} catch (Exception e) {
						subscriber.onError(e);
					}		
				}				
				subscriber.onCompleted();	
				executor.shutdownNow();
				return;
			}
		});    
	}
}
