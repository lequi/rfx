package rfx.core.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;

import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;


public class CmdProcessUtil {

	static class Worker extends Thread {
		private final Process process;
		private Integer exit;

		private Worker(Process process) {
			this.process = process;
		}

		@Override
		public void run() {
			try {
				exit = process.waitFor();
			} catch (InterruptedException ignore) {
				return;
			}
		}
	}

	public static String executePython3Script3(String cmd, int timeout) {
		Process process = null;
		Worker worker = null;
		String rs = StringPool.BLANK;

		try {
			Runtime runtime = Runtime.getRuntime();
			process = runtime.exec(cmd);

			worker = new Worker(process);
			worker.start();
			worker.join(timeout);
			if (worker.exit != null) {
				InputStream is = process.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line;
				// System.out.printf("Output of running %s is:",
				// Arrays.toString(args));
				StringBuilder s = new StringBuilder();
				while ((line = br.readLine()) != null) {
					s.append(line).append("\n");
				}
				rs = s.toString();
			} else {
				// timeout
				worker.notify();// tell the monitor thread that got timeout
								// event
				LogUtil.e("CmdProcessUtil", "timeout after " + timeout
						+ " milliseconds when exec python3 " + cmd);
			}
		} catch (Exception ex) {
			if (worker != null) {
				worker.interrupt();
			}
			Thread.currentThread().interrupt();
		} finally {
			if (process != null) {
				process.destroy();
			}
		}

		return rs;
	}
	
		
	public static String exec(String cmd) {
		String pid = ManagementFactory.getRuntimeMXBean().getName();
		//System.out.println(pid);
				
		String rs = StringPool.BLANK;
		Process p;
		try {
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
 
			StringBuffer sb = new StringBuffer();
 
			String line = reader.readLine();
			sb.append(line);
			while (line != null) {
				if(line != null){
					//System.out.println(line);
					line = reader.readLine();
					sb.append(line);
				}				
			}
			rs = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public static String executePython3Script2(String cmd, int timeout) {
		System.out.println(cmd);
		Process process = null;
		Worker worker = null;
		String rs = StringPool.BLANK;
		try {
			Runtime runtime = Runtime.getRuntime();
			process = runtime.exec(cmd);
			worker = new Worker(process);
			worker.start();
			worker.join(timeout);
			if (worker.exit != null) {
				InputStream is = process.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line;

				// System.out.printf("Output of running %s is:", Arrays.toString(args));
				StringBuilder s = new StringBuilder();
				while ((line = br.readLine()) != null) {
					s.append(line).append("\n");
				}
				rs = s.toString();
			} else {
				System.err.println("timeout");
			}
		} catch (Exception ex) {
			worker.interrupt();
			Thread.currentThread().interrupt();
		} finally {
			process.destroy();
		}
		return rs;
	}

	public static String executePython3Script(String cmd, int timeout) {
		System.out.println(cmd);
		Process process = null;
		Worker worker = null;
		StringBuilder s = new StringBuilder();
		try {
			process = Runtime.getRuntime().exec(cmd);
			worker = new Worker(process);
			worker.start();
			worker.join(timeout);
			if (worker.exit != null) {
				InputStream is = process.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line;

				// System.out.printf("Output of running %s is:",
				// Arrays.toString(args));

				while ((line = br.readLine()) != null) {
					s.append(line).append("\n");
				}
			} else {
				// timeout
				System.out.println("timeout");
				LogUtil.e("CmdProcessUtil", "timeout after " + timeout
						+ " milliseconds when exec python3 " + cmd);
				// worker.notify();//tell the monitor thread that got timeout
				// event
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (worker != null) {
				worker.interrupt();
			}
			Thread.currentThread().interrupt();
		} finally {
			if (process != null) {
				process.destroy();
			}
		}
		return s.toString();

	}

	public static void main(String args[]) throws IOException,
			InterruptedException {
		//String scriptpath = "python3 multilang/resources/hello.py abc xyz 111";
		//String scriptpath1 = "python3 scripts/fraud-click-filter/clickprocess.py 127.0.0.1 1300000 b90d94578d1a527500b3c0a2970f0a6515fcc70d 1321321 86";

		
		String html = exec("/home/trieu/Programs/phantomjs-1.9.7-linux-i686/bin/phantomjs /home/trieu/Programs/phantomjs-1.9.7-linux-i686/test-get-links.js");
		System.out.println(html);
		Thread.sleep(10000);
	}
}
