package rfx.core.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.CountingOutputStream;

public class FileIOUtil {
	

	public static class DownloadCountingOutputStream extends CountingOutputStream {
		int totalLen;

		public DownloadCountingOutputStream(OutputStream out) {
			super(out);
		}
		
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			super.write(b, off, len);
			totalLen += len;
			System.out.println("len:" + len);
		}
	}
	
	public static boolean downloadFile(String url, String filename){
		boolean ok = true;
		try {
			URL dl = null;
			File fl = null;			
			OutputStream os = null;
			InputStream is = null;

			try {
				fl = new File(filename);
				dl = new URL(url);
				os = new FileOutputStream(fl);
				is = dl.openStream();

				DownloadCountingOutputStream dcount = new DownloadCountingOutputStream(os);

				// this line give you the total length of source stream as a String.
				// you may want to convert to integer and store this value to
				// calculate percentage of the progression.
				String len = dl.openConnection().getHeaderField("Content-Length");
				
				// begin transfer by writing to dcount, not os.
				IOUtils.copy(is, dcount);
				
				System.out.println("Content-Length "+len);
				System.out.println("totalLen "+dcount.getByteCount());

			} catch (Exception e) {
				System.out.println(e);
				ok = false;
			} finally {
				if (os != null) {
					os.close();
				}
				if (is != null) {
					is.close();
				}				
			}
		} catch (Exception e) {
			ok = false;
		}
		return ok;
	}

	public static void main(String[] args) throws Exception {
		String filename = "libs/twitter-text-1.6.1.jar";
		String url = "http://repo1.maven.org/maven2/com/twitter/twitter-text/1.6.1/twitter-text-1.6.1.jar";
		downloadFile(url, filename );
	}
}
