package rfx.core.util.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import rfx.core.model.RawLogRecord;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;
import rfx.core.util.Utils;

public class HadoopUtil {

	static org.apache.hadoop.conf.Configuration config = new org.apache.hadoop.conf.Configuration();
	static FileSystem hdfs;
	static {
		config.addResource(new Path(FileUtils.getRuntimeFolderPath()
				+ "/configs/hadoop/core-site.xml"));
		config.addResource(new Path(FileUtils.getRuntimeFolderPath()
				+ "/configs/hadoop/hdfs-site.xml"));
		LogUtil.i("HadoopUtil",
				"config.addResource : " + FileUtils.getRuntimeFolderPath()
						+ "/configs/hadoop/core-site.xml");
		LogUtil.i("HadoopUtil",
				"config.addResource : " + FileUtils.getRuntimeFolderPath()
						+ "/configs/hadoop/hdfs-site.xml");
		_initHdfs();
	}

	private static void _initHdfs() {
		try {
			LogUtil.i("HadoopUtil", "call _initHdfs");
			hdfs = FileSystem.get(config);
		} catch (IOException e) {
			LogUtil.e("HadoopUtil", "init Hadoop Configuration fail : " + e);
			e.printStackTrace();
		}
	}

	/**
	 * Write to HaDoop. If the file is exist -> append, else -> create new file
	 * on HDFS
	 * 
	 * @param path
	 * @param fileName
	 * @param rawLogRecords
	 * @throws Exception
	 */
	public static synchronized void writeToHaDoop(String path, String fileName,	List<RawLogRecord> rawLogRecords) throws Exception {

		FSDataOutputStream outputStream = null;

		try {
			Path hdfsPath = new Path(path + fileName);

			if (hdfs == null) {
				_initHdfs();
			}

			if (!hdfs.exists(hdfsPath)) {
				hdfs.createNewFile(hdfsPath);
				System.out.println("file not exist, create new : " + hdfsPath);
				LogUtil.i("HadoopUtil",
						"writeToHaDoop, file not exist, create new : "
								+ hdfsPath, true);
			}
			outputStream = hdfs.create(hdfsPath);
			for (RawLogRecord logRecord : rawLogRecords) {
				outputStream.writeBytes(StringUtil.toString(
						logRecord.getLogData(), "\n"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.e("HadoopUtil", "writeToHaDoop error : " + e + ", path : "
					+ (path + fileName));
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		int unixtime = DateTimeUtil.currentUnixTimestamp();
	    String rawLogFileFolder = "/nguyentantrieu-info-access-logs/";
		String rawLogFileName = "Nov-2013";
		String log = FileUtils.readAbsolutePathFileAsString("/home/trieu/git-projects/data/nguyentantrieu.info-Nov-2013");
		
		RawLogRecord rawLogRecord = new RawLogRecord("0", unixtime, log);
		List<RawLogRecord> rawLogRecords = new ArrayList<>();
		rawLogRecords.add(rawLogRecord);
		HadoopUtil.writeToHaDoop(rawLogFileFolder, rawLogFileName, rawLogRecords);
		Utils.sleep(10000);
	}

}