package rfx.core.kafka;

import rfx.core.emitters.DataEmitter;

public abstract class KafkaDataEmitter extends DataEmitter {
		

	public KafkaDataEmitter() {
		super();
	}

	private KafkaDataSeeder kafkaDataSeeder;	
	
	public KafkaDataEmitter(KafkaDataSeeder kafkaDataSeeder) {
		super();
		this.kafkaDataSeeder = kafkaDataSeeder;
	}	
	
	public KafkaDataSeeder myKafkaDataSeeder() {
		return kafkaDataSeeder;
	}

	public KafkaDataEmitter setKafkaDataSeeder(KafkaDataSeeder kafkaDataSeeder) {
		this.kafkaDataSeeder = kafkaDataSeeder;
		return this;
	}
	
}
