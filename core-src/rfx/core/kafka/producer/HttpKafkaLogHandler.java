package rfx.core.kafka.producer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicLong;

import kafka.producer.ProducerConfig;
import rfx.core.configs.KafkaProducerConfigs;
import rfx.core.kafka.KafkaProducerUtil;
import rfx.core.util.LogUtil;

public class HttpKafkaLogHandler implements KafkaLogHandler {
	
	static KafkaProducerConfigs configs = KafkaProducerConfigs.load();	
	
	//TODO optimize this config
	public final static int MAX_KAFKA_TO_SEND = 900;
	public final static long TIME_TO_SEND = 5000; //in milisecond
	public static int NUM_BATCH_JOB = configs.getNumberBatchJob();
	public static int SEND_KAFKA_THREAD_PER_BATCH_JOB = configs.getSendKafkaThreadPerBatchJob();
		
	private static Map<String, HttpKafkaLogHandler> _kafkaHandlerList = new HashMap<>();
	
	AtomicLong counter = new AtomicLong();

	private List<LogBuffer> logBufferList = new ArrayList<>(NUM_BATCH_JOB);
	private Timer timer = new Timer();	
	private Random randomGenerator;
			
	private ProducerConfig producerConfig;

	private String topic = "";
	private int maxSizeBufferToSend = MAX_KAFKA_TO_SEND;
	
	public void countingToDebug() {
		long c = counter.addAndGet(1);
		System.out.println(this.topic + " logCounter: " + c);		
	}
	
	public static void initKafkaSession() {			
		try {
			Map<String,Map<String,String>> kafkaProducerList = configs.getKafkaProducerList();
			Set<String> keys = kafkaProducerList.keySet();
			System.out.println(keys);
			String defaultPartitioner = configs.getDefaultPartitioner();
			for (String key : keys) {
				Map<String,String> jsonProducerConfig = kafkaProducerList.get(key);
				String topic = jsonProducerConfig.get("kafkaTopic");
				String brokerList =  jsonProducerConfig.get("brokerList");			
				String partioner =  jsonProducerConfig.get("partioner");
				if (partioner == null ) {
					partioner = defaultPartitioner;
				}							
				Properties configs = KafkaProducerUtil.createProducerProperties(brokerList, partioner,MAX_KAFKA_TO_SEND);
				ProducerConfig producerConfig = new ProducerConfig(configs);
				HttpKafkaLogHandler kafkaInstance = new HttpKafkaLogHandler(producerConfig, topic);
				_kafkaHandlerList.put(key, kafkaInstance);
				LogUtil.i("KafkaHandler.init-loaded: "+ key + " => "+jsonProducerConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}		
	}
	
	public void closeKafkaSession() {
		//executor.shutdown();
		//producer.close();		
	}
	
	
	public static HttpKafkaLogHandler getKafkaHandler(String kafkaType){
		return _kafkaHandlerList.get(kafkaType);
	}
	
	protected HttpKafkaLogHandler(ProducerConfig producerConfig, String topic) {
		this.producerConfig 	= 	producerConfig;
		this.topic 				= 	topic;
		initTheTimer();
	}	
	
	
	public int getMaxSizeBufferToSend() {
		return maxSizeBufferToSend;
	}

	public void setMaxSizeBufferToSend(int maxSizeBufferToSend) {
		this.maxSizeBufferToSend = maxSizeBufferToSend;
	}

	void initTheTimer(){
		int delta = 400;
		for (int i = 0; i < NUM_BATCH_JOB; i++) {
			int id = i+1;
			LogBuffer buffer = new LogBuffer(producerConfig, topic,id);
			timer.schedule(buffer,delta , TIME_TO_SEND );
			delta += 100;
			logBufferList.add(buffer);
		}		
		randomGenerator = new Random();
	}	
	
	
	
	@Override
	public void writeLogToKafka(String ip, String userAgent, String logDetails, String cookieString){		
		countingToDebug();			
		int index = randomGenerator.nextInt(logBufferList.size());		
		
		try {
			LogBuffer logBuffer = logBufferList.get(index);
			if(logBuffer != null){
				logBuffer.push(new HttpLogMessage(ip, userAgent, logDetails, cookieString));
			} else {
				LogUtil.e(topic, "writeLogToKafka: FlushLogToKafkaTask IS NULL");
			}
		} catch (Exception e) {
			LogUtil.e(topic, "writeLogToKafka: "+e.getMessage());
		}
	}
	
	@Override
	public void flushAllLogsToKafka() {
		for (LogBuffer logBuffer : logBufferList) {
			//flush all
			logBuffer.flushLogsToKafkaBroker(false,-1);
		}		
	}

}
