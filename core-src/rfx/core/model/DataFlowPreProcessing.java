package rfx.core.model;

public abstract class DataFlowPreProcessing implements Callback<String>{
	public abstract CallbackResult<String> call();
}
