package rfx.core.model;

public abstract class DataFlowPostProcessing implements Callback<String>{
	public abstract CallbackResult<String> call();
}
