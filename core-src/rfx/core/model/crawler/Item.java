package rfx.core.model.crawler;

import rfx.core.util.DateTimeUtil;

import com.google.gson.Gson;

/**
 *
 * @author tantrieuf31
 */
public class Item implements Comparable<Item>{
    String url = "";
    String name = "";
    String imageUrl = "";
    int creationTime;
    SocialMetric socialMetric;
    Context context; 
    
    public Item(String url, String name) {
        this.url = url;
        this.name = name;
        creationTime = DateTimeUtil.currentUnixTimestamp();
    }

    public Item(String url, String name, String imageUrl) {
        this.url = url;
        this.name = name;
        this.imageUrl = imageUrl;
        creationTime = DateTimeUtil.currentUnixTimestamp();
    }
    

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(int creationTime) {
        this.creationTime = creationTime;
    }

    public SocialMetric getSocialMetric() {
        return socialMetric;
    }

    public void setSocialMetric(SocialMetric socialMetric) {
        this.socialMetric = socialMetric;
    }
    
    public void setTotalSocialScore(int totalSocialScore) {
    	if(this.socialMetric == null){
    		this.socialMetric = new SocialMetric();
    	}
        this.socialMetric.setTotalSocialScore(totalSocialScore);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    
    public void setContextLocation(String location) {
    	if(this.context == null){
    		this.context = new Context();
    	}
        this.context.setLocation(location);
    }
    
    

    public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getImageUrl() {
		return imageUrl;
	}


	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
    public int compareTo(Item o) {
		if(this.socialMetric.getTotalSocialScore() > o.getSocialMetric().getTotalSocialScore()){
			return 1;
		}
		if(this.socialMetric.getTotalSocialScore() < o.getSocialMetric().getTotalSocialScore()){
			return -1;
		}
		return 0;
    }
	

    @Override
    public String toString() {
    	return new Gson().toJson(this);
    }
}
