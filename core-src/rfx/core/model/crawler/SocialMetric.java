/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rfx.core.model.crawler;

import com.google.gson.Gson;

/**
 *
 * @author tantrieuf31
 */
public class SocialMetric {
    int facebookLikeCount;
    int facebookShareCount;
    int facebookCommentCount;
    int twitterCount;
    
    int totalSocialScore;

    public int getFacebookLikeCount() {
        return facebookLikeCount;
    }

    public void setFacebookLikeCount(int facebookLikeCount) {
        this.facebookLikeCount = facebookLikeCount;
    }

    public int getFacebookShareCount() {
        return facebookShareCount;
    }

    public void setFacebookShareCount(int facebookShareCount) {
        this.facebookShareCount = facebookShareCount;
    }

    public int getFacebookCommentCount() {
        return facebookCommentCount;
    }

    public void setFacebookCommentCount(int facebookCommentCount) {
        this.facebookCommentCount = facebookCommentCount;
    }

    public int getTwitterCount() {
        return twitterCount;
    }

    public void setTwitterCount(int twitterCount) {
        this.twitterCount = twitterCount;
    }

    public int getTotalSocialScore() {
        if(totalSocialScore == 0){
            totalSocialScore = this.facebookLikeCount + this.facebookShareCount;
            totalSocialScore += this.facebookCommentCount;
            totalSocialScore += this.twitterCount;
        }
        return totalSocialScore;
    }

    public void setTotalSocialScore(int totalSocialScore) {
        this.totalSocialScore = totalSocialScore;
    }

    @Override
    public String toString() {
    	return new Gson().toJson(this);
    }
}
