package rfx.core.model;

import java.io.Serializable;

public class SystemEventData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2037868497654923504L;
	int order;
	String source;
	long loggedTime;
	String eventDetails;

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public long getLoggedTime() {
		return loggedTime;
	}

	public void setLoggedTime(long loggedTime) {
		this.loggedTime = loggedTime;
	}

	public String getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(String eventDetails) {
		this.eventDetails = eventDetails;
	}

}
