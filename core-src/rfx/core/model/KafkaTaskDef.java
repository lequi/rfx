package rfx.core.model;

public class KafkaTaskDef implements TaskDef{
	String topic;
	int beginPartitionId, endPartitionId;
	boolean autoStart;
	
	
	public KafkaTaskDef(String topic, int beginPartitionId, int endPartitionId,	boolean autoStart) {
		super();
		this.topic = topic;
		this.beginPartitionId = beginPartitionId;
		this.endPartitionId = endPartitionId;
		this.autoStart = autoStart;
	}


	public String getTopic() {
		return topic;
	}


	public void setTopic(String topic) {
		this.topic = topic;
	}


	public int getBeginPartitionId() {
		return beginPartitionId;
	}


	public void setBeginPartitionId(int beginPartitionId) {
		this.beginPartitionId = beginPartitionId;
	}


	public int getEndPartitionId() {
		return endPartitionId;
	}


	public void setEndPartitionId(int endPartitionId) {
		this.endPartitionId = endPartitionId;
	}


	public boolean isAutoStart() {
		return autoStart;
	}


	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
	}
	
	
}
