package rfx.system.starter;

import java.io.IOException;
import java.util.ServiceLoader;

import rfx.core.cluster.node.BaseWorker;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.util.LogUtil;

/**
 * the worker class for starting automatically by supervisor
 * 
 * @author trieu
 *
 */
public class WorkerNodeStarter {
	
	public static <T> T loadService(Class<T> api) {
		 
        T result = null;
 
        ServiceLoader<T> impl = ServiceLoader.load(api);
 
        for (T loadedImpl : impl) {
            result = loadedImpl;
            if ( result != null ) break;
        }
 
        if ( result == null ) throw new RuntimeException(
            "Cannot find implementation for: " + api);
 
        return result;
 
    }
	
	public static void main(String[] args) throws IOException {
		ConfigAutoLoader.loadAll();
		LogUtil.i("--JVM: " + System.getProperty("sun.arch.data.model") + " bit, version: " + System.getProperty("java.version"));
		
		BaseWorker worker = loadService(BaseWorker.class);  
		
		System.out.println(worker.getName()+"");
		
//		if (args.length == 0) {			
//			//ready for get tasks from master
//			KafkaProcessorNode.startWorker();			
//		} else if (args.length == 1) {			
//			//start by registered name
//			String workerName = args[0];
//			KafkaProcessorNode.startWorker(workerName);			
//		} else if (args.length >= 4) {
//			// at least 4 params to run
//			String workerName = args[0];
//			String topic = args[1];
//			int beginPartitionId = StringUtil.safeParseInt(args[2], -1);
//			int endPartitionId = StringUtil.safeParseInt(args[3], -1);
//			
//			if(StringUtil.isEmpty(workerName) ){
//				System.err.println("Invalid workerName:"+workerName);
//				return;
//			}
//			if(StringUtil.isEmpty(topic) ){
//				System.err.println("Invalid topic:"+topic);
//				return;
//			}
//			if(beginPartitionId <0 ){
//				System.err.println("Invalid beginPartitionId:"+beginPartitionId);
//				return;
//			}
//			if(endPartitionId <0 ){
//				System.err.println("Invalid endPartitionId:"+endPartitionId);
//				return;
//			}
//			
//			//ready to go
//			KafkaProcessorNode.startWorker(workerName, topic, beginPartitionId, endPartitionId);
//			
//		} else {
//			System.err.println(" Try the syntax: java -jar worker.jar ");
//		}
	}
}
