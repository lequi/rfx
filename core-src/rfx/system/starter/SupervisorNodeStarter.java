package rfx.system.starter;


import rfx.core.cluster.node.SupervisorNode;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.util.LogUtil;
import rfx.core.util.Utils;

//http://steveliles.github.io/invoking_processes_from_java.html
public class SupervisorNodeStarter {
	
	static boolean working = true;
	public static void main(String[] args) throws Exception {
		ConfigAutoLoader.loadAll();
		boolean autoHeal = true;
		if(args.length>0){
			autoHeal = args[0].equalsIgnoreCase("autoheal:true");
		}			
		String nodeId = "supervisor-node";
		LogUtil.setSuffixLogFile(nodeId);
		LogUtil.setLogFileHourly(false);
		System.out.println("------autoheal:"+autoHeal);
		SupervisorNode.start(autoHeal);
		
		while(working){
			Utils.sleep(1000);
		}
	}
}
