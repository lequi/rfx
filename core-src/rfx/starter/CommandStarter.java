package rfx.starter;

import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.message.WorkerPayload;
import rfx.core.util.SupervisorUtil;

public class CommandStarter {

	public static void main(String[] args) {
		ConfigAutoLoader.loadAll();
		if(args.length == 1){
			String cmd = args[0];
			if(cmd.equalsIgnoreCase(WorkerPayload.SHUTDOWN_ALL_WORKERS)){
				SupervisorUtil.killAllWorkers(false);
			}
		} else if (args.length == 2) {
			String receivedWorkerName = args[0];
			String cmd = args[1];
			if(cmd.equalsIgnoreCase(WorkerPayload.SHUTDOWN)){
				SupervisorUtil.kill(receivedWorkerName);
			}
		} 
	}

}
