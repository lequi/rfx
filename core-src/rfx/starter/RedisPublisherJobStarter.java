package rfx.starter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import redis.clients.jedis.Jedis;
import rfx.core.configs.ClusterInfoConfigs;
import rfx.core.configs.loader.ConfigAutoLoader;
import rfx.core.nosql.jedis.RedisInfo;
import rfx.core.util.StringUtil;
import rfx.core.util.Utils;

import com.google.gson.Gson;

public class RedisPublisherJobStarter {

    public static final String CHANNEL_COMPACT_STATS = "compactStatsChannel";
    public static final String CHANNEL_FULL_STATS = "fullStatsChannel";
    
    static Timer timer = new Timer(true);
    final static RedisInfo clusterredisInfo;
    volatile static Jedis publisherJedis;
    
    static {
    	ConfigAutoLoader.loadAll();		
		ClusterInfoConfigs clusterInfoConfigs = ClusterInfoConfigs.load();
		clusterredisInfo = clusterInfoConfigs.getClusterInfoRedis();	
    }
    
    static void startRealtimeDataPublisher(String[] args){
    	System.out.println(args.length);
		if(args.length < 6){
			System.out.println("Use default: 10.254.53.17 45386 10.254.53.17 45381 10.254.53.17 45383");
			args = "10.254.53.17 45386 10.254.53.17 45381 10.254.53.17 45383".split(" ");
			//return;
		}
		final String hostPageviewImpression = args[0];
		final int portPageviewImpression = StringUtil.safeParseInt(args[1]);
		
		final String hostClick = args[2];
		final int portClick = StringUtil.safeParseInt(args[3]);
		
		final String hostTrueImpression = args[4];
		final int portTrueImpression = StringUtil.safeParseInt(args[5]);
			
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
				try {									
					if(publisherJedis == null){
						publisherJedis = new Jedis(clusterredisInfo.getHost(),clusterredisInfo.getPort(),0);
						publisherJedis.connect();
					}
		
//					Map<String, Object> map = RealtimeAnalyticDao.get().buildCompactData(hostPageviewImpression, portPageviewImpression, hostClick, portClick, hostTrueImpression, portTrueImpression);
//					String message = new Gson().toJson(map);
//					long rs = publisherJedis.publish(CHANNEL_COMPACT_STATS, message);
//					System.out.println(rs +" publisherJedis.publish: "+message);
					
				} catch (Exception e) {					
					e.printStackTrace();
					try {
						publisherJedis.disconnect();					
						publisherJedis = null;
						Utils.sleep(500);
					} catch (Exception e1) {}
				}				
			}
		}, 3000, 1500);
    }
    
    static void testRealtimePublisher(){
    	timer.schedule(new TimerTask() {			
			@Override
			public void run() {
				try {									
					if(publisherJedis == null){
						publisherJedis = new Jedis(clusterredisInfo.getHost(),clusterredisInfo.getPort(),0);
						publisherJedis.connect();
					}
					Map<String, Object> map = new HashMap<String, Object>(12);
					Date d = new Date();					
					DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
					map.put("datehourval", dateFormat.format(d));
					
					map.put("total-impression-1hour",  System.currentTimeMillis() );
					
					String message = new Gson().toJson(map);
					publisherJedis.publish(CHANNEL_COMPACT_STATS, message);
					
					int sleepRandom = (int)(Math.random() * (4 + 1));
					Utils.sleep(sleepRandom*1000);
				} catch (Exception e) {					
					e.printStackTrace();					
				}				
			}
		}, 1000, 1000);
    }
    
	public static void main( String[] args) {
		
		//startRealtimeDataPublisher(args);
		testRealtimePublisher();
		while(true){
			Utils.sleep(5000);
		}
	}
}
