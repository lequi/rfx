
import groovyx.gpars.GParsPool;



import java.util.concurrent.atomic.AtomicInteger;

def s = "";

def getList() {
	def list = []
	list << "element 1"
	list << "element 2"
	list << "element 3"
}

getList().each({ el ->
	s += ( el.toUpperCase() + "<br>");
})


def myNumbers = 0;
GParsPool.withPool {
     final AtomicInteger result = new AtomicInteger(0)
     [1, 2, 3, 4, 5].eachParallel {result.addAndGet(it)}
     assert 15 == result
	 myNumbers = [1, 2, 3, 4, 5].parallel.reduce {a, b -> a + b}
 }
output = s + params['p']?.getAt(0) + params['b']?.getAt(0) + "<br>myNumbers: "+ myNumbers;


class DownloadHelper {
	String download(String url) {
		String text = url.toURL().text
		println text
		return text
	}
	int scanFor(String word, String text) {
		text.findAll(word).size()
	}

	String lower(s) {
		s.toLowerCase()
	}
}
//now we'll make the methods asynchronous
GParsPool.withPool {
	final DownloadHelper d = new DownloadHelper()
	Closure download = d.&download.asyncFun()
	Closure scanFor = d.&scanFor.asyncFun()
	Closure lower = d.&lower.asyncFun()

	//asynchronous processing
	def result = scanFor('groovy', lower(download('http://www.infoq.com/groovy')))
	println 'Allowed to do something else now'
	println result.get()
}
