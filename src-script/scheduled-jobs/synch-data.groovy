import groovy.json.JsonBuilder;
import groovy.json.*
import static groovy.json.JsonParserType.*


output = "Run synch-impression-pageview.groovy OK"

def a = [:]
a["a"] = 6
a['b'] = 2

println a['a'] + a['b']

(['a','b'].each { println it })

println new JsonBuilder( a ).toPrettyString()

println "synched-time: " + new java.util.Date().toString()

def parser = new JsonSlurper().setType(LAX)

def conf = parser.parseText '''
    // configuration file
    {
        // no quote for key, single quoted value
        environment: 'production'
        # pound-style comment
        'server': 5
    }
'''
println conf.server


